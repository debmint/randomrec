Record

This is a simple records-keeping program initially designed for Microware OS9 and OSK, although it was later made to be used with other operating systems.

It uses random-access disk files to store data - I didn't have any good databases then, and they still are scarce and as of now almost impossible to get for this platform.

Try it and play around with it if you would like.
