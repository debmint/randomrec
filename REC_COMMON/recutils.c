/* Utilities   Note: these could be made universal
 */

#include "../record.h"
#include <ctype.h>

extern char *BL;

extern Bell(x);

char yesno()
{
char inkey;

   do {
      read ( 0,&inkey,1);
   } while ( !strchr("YN",(inkey = toupper(inkey))) );
   return(inkey);
}

static char rbuf;

char getch()
{
	read(0,&rbuf,1);
	return rbuf;
}

stripcr(line)
 char *line;
{

 register char *cloc=line;

   cloc+=(strlen(line)-1);           /* Point to last character in string */
   if ( *cloc == '\x0d' ) {
      *cloc = '\x0';    /* IF CR, replace with null   i.e.           */
                        /*  in BASIC, line = left$(line,len(line-1)) */
   }
}

/* "getstr()" does a readln from stdin, cuts CR from end-of-string
 *   call: getstr(string,max-len)
 *   returns: 0 on error, otherwise ptr to string
 */

getstr(line,lgth)
 char *line;
 int lgth;
{
 register unsigned int linlen = 0;
 register char *by = line;

	setecho(0);		/* Turn off screen echo */
	--lgth;			/* Reserve space for Null string terminator */
	for( ;;) {
		if ( read(STDIN,by,1) == -1 ) return 0;
		if ( *by == '\x0d' ) {
			*by = NULL;		/* Null terminate string */
			break;
		}
		if( isprint(*by) ) {
			if( linlen < lgth ) {
				writeln(STDOUT,by++,1);
				++linlen;
			}
			else {
				Bell(STDOUT);
			}
		}
		else {
			if( *by == '\x08' ) {	/* Assumption for backspace here */
				if( linlen ) {
					writeln(STDOUT,by,1);
					writeln(STDOUT," ",1);
					writeln(STDOUT,by--,1);
					--linlen;
				}
			}
			else {
				if( *by == '\x1b' ) {	/* ESCape sequence */
					if( read(STDIN,by,1) == -1)
						return(0);
					if( *by == '[' )	/* 3-char ESC sequence? */
						if( read(STDIN,by,1) == -1 )
							return(0);
				}
			}
		}
	}
	*by = '\x0';
	setecho(1);		/*	Restore screen echo */
	return(line);
}

/* NOTE!!!  This is a first version of "getstr()".. above is an
	improvement (hopefully)
/* "getstr()" does a readln from stdin, cuts CR from end-of-string
 *   call: getstr(string,max-len)
 *   returns: 0 on error, otherwise ptr to string
 */

/*getstr1(line,lgth)
 char *line;
 int lgth;
{
 register int linlen = 0;
 register char *by = line;
 register char *by2 = line;


	if ( (linlen=readln(0,line,lgth)) < 1 )
		return(0);


	line[linlen] = '\x0';
	return(line);
}*/

	/* keyrd()	: Reads for a keypress
	 *
	 * Returns	: The key pressed
	 */

char keyrd()
{
	char iky;
	
	read(0,&iky,1);   /* Get a keypress */
	return(iky);      /* return it to caller */
}



#ifndef _OSK
/* gtime()   get system time
 *    Entry:   address for time buffer
 *    Returns: address of this buffer
 */
/* Delete for now: coco HAS a getime
 * why did I do this to start with???
gtime()
{
#asm
F$Time equ $15
 ldx 4,s   Address of buffer
 os9 F$Time
 bcs err
 ldd 4,s
 bra gottim
err clra
 stb errno,u
 ldd #-1
gottim
#endasm
}*/
#endif

/* center(int path,char *string,int colwidth): centers a string over
         a given space (from the current print position).
    If the string is longer than the line, it will return -1 without
    printing.
   RETURNS: -1 on error or string length longer than the width.

   !!!   NOTE:  pthnum is OS9 path #, NOT FILE *   !!!!!
*/

center(pthnm,str,wdth)
int  pthnm;
char *str;
int  wdth;
{
   register int lsp;        /* # spaces to print on left of string */
            int rsp;        /* # spaces to print after string      */
            char *spac=" ";

   if (wdth<strlen(str))
      return(-1);
          /* Get left-right space count */
   lsp=(wdth-strlen(str))/2;
   rsp=wdth-strlen(str)-lsp;
   while (lsp--) {                  /* do left spaces */
      if (write(pthnm,spac,1)==-1)
         return(-1);
   }
   if(write(pthnm,str,strlen(str))==-1)
      return(-1);
   while(rsp--) {                   /* do right spaces */
      if (write(pthnm,spac,1)==-1)
         return(-1);
   }
   return(0);
}


findustr(str,pat)
char *str,*pat;
{
	register char *pos;
	register int  c1;
	
	if(strlen(pat) > strlen(str) )
		return 0;  /* error: pattern too big for string */

	c1=0;
	for( pos=str;;pos++ ) {
		if(*pos=='\0')
			return 0;
		++c1;
		if( !docmp(pos,pat) )
			return c1;
	}
	return 0;
}

docmp(MainS,patS)
char *MainS, *patS;
{
	register char *c1, *c2;
	char patc, mainc;
	
	c1=MainS; c2=patS;
	while ( patc = *(c2++) ) {
		if( isalpha(patc) && islower(patc) )
			mainc= tolower(*(c1++));
		else
			mainc= *(c1++);
		if( mainc != patc )
			return -1;
	}
	return 0;
}

