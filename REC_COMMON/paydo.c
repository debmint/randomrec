/* Payor functions (for "record").  display list, add to list
*/
#ifdef _OSK
#include "../record.h
#else
#include "../record.h"
#endif

pyrshow()
{
register int count;

   cls;
   printf("%24s\n\n","P A Y O R / P A Y E E   L I S T");
   for ( count = 0; count < NumPayrs; count++ ) {
      CurXY( scrn, 26*(count/((NumPayrs+2)/3)), (count%((NumPayrs+2)/3))+3 );
      printf("%02d-%-22s",count+1,Person[count].Who);
   }
   CurXY(scrn,0,22);
}

addpyr()
{
char inpstr[sizeof(Person[1].Who)+1];
register int ppath;

	pyrshow();
	CurXY(scrn,0,21);
	printf("Enter Name for new Payor/Payee...\n<ENTER> alone to exit...");

	if ( getstr(inpstr,sizeof(inpstr)) ) {    /* If a name was added*/
   		if( strlen(inpstr) == 0 )
   			return 0;
		strcpy(Person[NumPayrs].Who, inpstr);
		Person[NumPayrs].GotFrom = Person[NumPayrs].PdTo = 0;

	/* If in "record", write this one, else if in "recinit", wait
	 *  and write all at one time -- "fwrt" will be a null subroutine
	 */
		fwrt(payfil,&Person[NumPayrs],sizeof(Person[0]));
		return(&Person[NumPayrs++]);  /* return ptr to entry & bump count */
	}
	else {
		return(-1);           /* Return null if no addition */
	}
}                         /* End of addpyr()   */

