/* Cgy edit functions:   Display cagegories, add categories,
   warn if max # has been reached
*/

#include "../record.h"

cgyshow()
{
register int count;     /* for.. next counter */

   cls;
   printf("%15sI N C O M E / E X P E N S E   C A T E G O R I E S\n"," ");
        /*  Columnize Income Classes   */
#ifndef _OSK
   FColor(scrn,3); BColor(scrn,2);   /*  Green on Black */
#endif
   printf("Income Classes___\n");
#ifndef _OSK
   FColor(scrn,0); BColor(scrn,1);   /*  Normal colors  */
#endif
   for ( count = 0; count < InCgyNum; count++ ) {
      CurXY( scrn, 26*(count/((InCgyNum+2)/3)), (count%((InCgyNum+2)/3))+3 );
      printf("%2d-%-22s",count+1,Income[count].CgyNam);
   }
        /*  Columnize Expense Classes   */
   CurXY(scrn,0,8);
#ifndef _OSK
   FColor(scrn,3); BColor(scrn,2);
#endif
   printf("Expense Classes___\n");
#ifndef _OSK
   FColor(scrn,0); BColor(scrn,1);
#endif
   for (count = 0; count < ExCgyNum; count++ ) {
      CurXY( scrn, 26*(count/((ExCgyNum+2)/3)), (count%((ExCgyNum+2)/3))+9 );
      printf("%2d-%-22s",InCgyNum+count+1,Expense[count].CgyNam);
   }
}

addcgy()                  /* Add a new category to file    */
{
char inpstr[sizeof(Income[1].CgyNam)+1];
register char *wstr;
register int  *cgysiz, count, cpath;
long fsiz;
register struct cgystr *tmpcgy;

   cgyshow();
#ifndef _OSK
   OWSet(scrn,0,3,20,74,3,3,2); cls;
#else
   printf ("\n\n");
#endif

   printf("0=quit/done   I=add INCOME cgy    X=add EXPENSE cgy >>  ");
   do {
      printf ("\b");
   } while ( !strchr("0IiXx",(inpstr[0]=getchar() )) );
   printf("\n");

   /* First set up for add */
   switch (inpstr[0]) {
      case '0':
#ifndef _OSK
         OWEnd(scrn);
#endif
         return(0);
      case 'I':
      case 'i':
        wstr = "Income"; tmpcgy = Income; cgysiz = &InCgyNum;
        if (InCgyNum >= sizeof(Income)/sizeof(Income[1])) {
          cgyfull(wstr);
#ifndef _OSK
          OWEnd(scrn);
#endif
          return(0);
        }
        break;
      case 'X':
      case 'x':
        wstr = "Expense"; tmpcgy = Expense; cgysiz = &ExCgyNum;
        if (ExCgyNum >= sizeof(Expense)/sizeof(Expense[1])) {
          cgyfull(wstr);
#ifndef _OSK
          OWEnd(scrn);
#endif
          return(0);
        }
        break;
      default:
         break;
   }
   /* we are now set to go on and add one */
   printf( "Add %s category... \n[ENTER] alone to abort...",wstr);
   do {
     getstr(inpstr,sizeof(inpstr));
     if (strlen(inpstr) >= sizeof(Income[1].CgyNam )) {
       printf ("Too long.. Limit to %d chars\n",
        sizeof(Income[1].CgyNam-1));
     }
   } while (strlen(inpstr) >= sizeof(Income[1].CgyNam));
   if ( !strlen(inpstr) ) {
#ifndef _OSK
      OWEnd(scrn);
#endif
      return(0);
   }
   /* Now check to see if this name matches any already there */
   count = 0;
   while ( count++ < *cgysiz ) {
      if ( !strucmp(inpstr,(tmpcgy++)->CgyNam) ) {
         printf("%s is already recorded.  Aborting...\n Press [ENTER]..",
          inpstr);
         inpstr[0] = '\d0';   /* Null string to flag match */
         getchar();
#ifndef _OSK
         OWEnd(scrn);
#endif
         return(inpstr);
      }
   }
   /* System all set to save new entry... Verify you really want to */
   /* Note: at this point, tmpcgy is pointing to the first empty
      spot in the Catetory array                                    */
#ifndef _OSK
   cls;
#endif
   printf("Add");
   ReVOn(STDOUT);
   printf(" %s ",inpstr);
   ReVOff(STDOUT);
   printf("to... %s Are you sure <Y/N> ",wstr);
/*   printf("Add\x1f\x20 %s \x1f\x21to... %s Are you sure <Y/N>",inpstr,wstr );
*/
   ans=yesno();
   if ( ans != 'Y' ) {
#ifndef _OSK
      OWEnd(scrn);
#endif
      inpstr[0] = '\d0';
      return(inpstr);
   }
   else {
      ++*cgysiz;   /* Bump size of category (whichever it is) */
      strcpy ( tmpcgy->CgyNam,inpstr );
      tmpcgy->CgyTot = 0;
                 /* Now, if we are in "record", save it because
                    it is probably a one-shot deal.  If in recinit,
                    wait, write whole file at one time
                 */
/* make either "income[s]." or "expenses[s]."   */
		fwrt(strcpy(inpstr,!strcmp(wstr,"Income") ? incmfil : expfil),
			tmpcgy,sizeof(Income[0]));
#ifndef _OSK
      OWEnd(scrn);
#endif
      return(tmpcgy);      /* ... return ptr to this addition   */
   }
}                  /* End of addcgy */

cgyfull(wstr)
char *wstr;
{
   printf ("The %s Category list is full...\n",wstr);
   printf ("Aborting add process.  Press [ENTER]\n");
   getchar();
}

