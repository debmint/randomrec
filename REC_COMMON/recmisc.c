/* Miscellaneous routines used in record program */

#include "../record.h"

#ifdef _OSK
#include <termcap.h>

char *strchr();
extern char getch();

/*   termcap  entries     */
#define TCAPSLEN 400

extern char **environ, *getenv();
static char tcapbuf[TCAPSLEN];

char PC_,      /* Pad character */
     *BC,      /*     backspace */
     *UP;      /*     cursor up */
short ospeed;  /* Terminal speed*/
char *CM,
     *CL,
     *CE,
     *CD,
     *SO,
     *SE,
     *BL,
     *VE,
     *VI,
     *KD,
     *KU;
int nrows,
    ncolumns,
    standout;

#else                /*   CoCo     */

#include <lowio.h>

char *KD = "\x0a",
	 *KU = "\x0c";

#endif               /*  ifdef _OSK  */

static int NewPth;         /* path # for working window */
static int Old_Out;         /* save original stdout for screen reset */

/* setecho() Turns screen echo on/off depending on value of Flg */

setecho(Flg)
int Flg;
{
 Opts.sg_echo = (char)Flg;
 _ss_opt(STDOUT,&Opts);                 /* Setstat new opts into system */
}

recsetup()
{
 register char *cmps="CONDENSON=",
    *uncmp="CONDENSOFF=";

         /* First, disable keyboard interrupts in home window */
         /* For some reason, when this is done, the original
          *  screen is skipped in the 'clear' screen-select, which
          *  is desirable
         */

   _gs_opt(STDOUT,&OldOpts);        /* Get opts to save for restore on exit */
   _gs_opt(STDOUT,&Opts);           /* Get opts for current screen          */
                     /* Disable eof, kbd intrpt, abort, xon, xoff   */
 Opts.sg_eofch=Opts.sg_kbich=Opts.sg_kbich=Opts.sg_kbach=
    Opts.sg_pause=Opts.sg_xon=Opts.sg_xoff = 0;

   _ss_opt(STDOUT,&Opts);                 /* Setstat new opts into system */

         /* Now, set up new window */
#ifdef _OSK
   init_term_cap();

#else
   if ( (Old_Out = dup(STDOUT)) == ERR ) {
      exit( _errmsg(errno,"Can't save Original STDOUT\n"));
   }
   if ( (NewPth = open("/w",READ+WRITE)) == ERR ) {
      exit(_errmsg(errno,"can't open working window\n"));
   }
   if ( DWSet( NewPth,2,0,0,80,24,0,1,1 ) == ERR ) {
      exit(_errmsg(errno, "can't DWSet working window\n"));
   }
   Select(NewPth);

       /* For now, don't dupe original stdio.  I don't think that
        * they will be needed anymore while in process
        */
   close(STDIN); dup(NewPth);
   close(STDOUT); dup(NewPth);
   close(STDERR); dup(NewPth);
             /* Now, set Opts for new window to suit */

   _gs_opt(STDOUT,&Opts);                 /* Get opts for screen          */
   Opts.sg_kbich=Opts.sg_kbach = 0;       /* Disable kbd intrpt & abort   */
   _ss_opt(STDOUT,&Opts);                 /* Setstat new opts into system */
#endif             /*   ifdef _OSK    */


/*#ifdef _OSK*/
   if ( getime(tmpstr) !=ERR )
/*#else

 *   If CoCo works with getime(), delete all these conditionals
 *   from file later !!!!!!!!!!
   if ( gtime(tmpstr) != ERR )           /* Get today's date             
#endif*/
      memcpy (&Today,tmpstr,sizeof(Today));

   if ( !(fpath=fopen("pdvrs.rec","r")) )
      return(_errmsg(errno,"Cannot open \"pdvrs.rec\""));
   ibm = FALSE;
   while(fgets(tmpstr,sizeof(tmpstr),fpath)) {
      if(!strncmp(tmpstr,cmps,strlen(cmps))) {
         getpenv(&Cmprs,strchr(tmpstr,'=')+1);
      }
      else {
         if(!strncmp(tmpstr,uncmp,strlen(uncmp))) {
            getpenv(&Uncmprs,strchr(tmpstr,'=')+1);
         }
         else {
          if( !strncmp(tmpstr,"IBM",strlen("IBM")) ) {
           ibm = TRUE;
          }
        }
      }
   }
   fclose(fpath);
}

   /* getpenv: reads printer code file and sets up the printer
    *         control code in the code structure (see struct PZS
    *         in "record.h".        */

static getpenv(cp,pstart)
 PZS *cp;
 char *pstart;

{
   int tc[20];  /* tmp integer struct for scanf */
   register int gc;
   cp->ccount= sscanf(pstart,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    &tc[0],&tc[1],&tc[2],&tc[3],&tc[4],&tc[5],&tc[6],&tc[7],&tc[8],&tc[9]);

      /* Now convert tc-type ints to char cp->ccode */
   for ( gc=0; gc<cp->ccount; gc++ )
      cp->ccod[gc] = tc[gc];
    
}

               /* Exit routine */

quitrec()
{
#ifndef _OSK
            /* reset stdout */
            /* no need to close newscreen, will close automatically
             * on exit
             */
   close(STDOUT);
   dup(Old_Out);
#endif         /*  end coco */

   _ss_opt(STDOUT,&OldOpts); /* Reset std opts for path */
   cls;
   exit(errno);
}

#ifdef _OSK
extern char *tgetstr();

static init_term_cap()
{
 register char *term_type;
 char tcbuf[1024];
 char *ptr=tcapbuf;
 register char *temp;

  if ( (term_type = getenv("TERM")) == 0 ) {
     _errmsg ( errno=1, "Environment variable `TERM' not defined:\n");
     quitrec();
  }
  if ( tgetent(tcbuf,term_type)<=0 ) {
     _errmsg( errno=1, "Unknown terminal type `%s'!\n",term_type);
     quitrec();
  }
  if (temp=tgetstr("PC",&ptr))  PC_=*temp;
  CL=tgetstr("cl",&ptr);
  CM=tgetstr("cm",&ptr);
  CE=tgetstr("ce",&ptr);
  CD=tgetstr("cd",&ptr);
  SO=tgetstr("so",&ptr);
  SE=tgetstr("se",&ptr);
  BL=tgetstr("bl",&ptr);
  VI=tgetstr("vi",&ptr);
  VE=tgetstr("ve",&ptr);
  KD=tgetstr("kd",&ptr);
  KU=tgetstr("ku",&ptr);
  if ( ptr >= &tcapbuf[TCAPSLEN] ) {
     _errmsg ( errno=1, "Terminal description too big!\n");
     quitrec();
  }
  if ( !CM ) {
     _errmsg( errno=1, "termcap entry needs cursor movement!\n");
     quitrec();
  }
  nrows = tgetnum("li");
  ncolumns=tgetnum("co");
  if ( !nrows || !ncolumns ) {
     _errmsg(errno=1, "Unable to determine screen size!\n");
     quitrec();
  }
  if (SO && SE ) ++standout;     /* signal reverse video available */
}

int tputc(c)
 char c;
{
   return write(1,&c,1);
}

putpad(s)
 char *s;
{
   if (s) tputs(s,1,tputc);
}

Clear()
{
   if (CL) {
      putpad(CL);
   }
   else {
      CurXY(0,0,0);
      ErEOScrn(x);
      CurXY(0,0,0);
   }
}

#endif       /* end OSK termcap defs */

opn_err(fil)
 char *fil;
{
   getch(_errmsg(1,"Cannot open: %s\nPress [ENTER]...",fil));
}

rd_err(fil)
 char *fil;
{
   getch(_errmsg(1,"Read error: %s\nPress [ENTER]...",fil));
}

wrt_err(fil)
 char *fil;
{
   getch(_errmsg(1,"Write error: %s\nPress [ENTER]...",fil));
}

clos_err(pntr)
 char *pntr;
{
   getch(_errmsg(1,"Error in file close: %s\nPress [ENTER]...",pntr));
}


/* Here some defs for OSK that are already lib functions for coco. */

#ifdef _OSK

strucmp(s,t)
char *s,*t;
{
 char S,T;
    for ( ; (S=toupper(*(s++))) == (T=toupper(*(t++))); )
       if ( S == '\0' ) return 0;
    return s[-1] - t[-1];
}

char *strchr(st,c)
 char *st,
        c;

{
 char c1;
   while ( c1 = *(st++) ) {
      if ( c1 == c )
         return --st;
   }
   return 0;
}

void reverse();

char *itoa(n, s)
 int n;
 char s[];
{
 register int i, sign;

   if ( (sign = n) < 0 )
      n = -n;    /* abs(n) */
   i=0;
   do {    /* generate digits in reverse order */
      s[i++] = n % 10 + '0';    /* get next digit */
   } while ((n /= 10) > 0);    /* delete it */
   if ( sign < 0 )
      s[i++] = '-';
   s[i] = '\0';           /* null terminator for string */
   reverse(s);
   return s;
}

void reverse(s)
 char *s;
{
 register int c,i,j;

   for ( i=0, j=strlen(s)-1; i < j;  i++,j--) {
      c=s[i];
      s[i] = s[j];
      s[j] = c;
   }
}

#endif        /* end OSK */
