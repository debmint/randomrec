/* ********************************************** *
 * widget_ring_prev() - updates window to go to   *
 *    previous widget in ring                     *
 * Passed: WINDOW in which widgets are placed     *
 *         WDGT_RING in which widget resides      *
 *         current widget                         *
 * ********************************************** */

WDGT_RING *
widget_ring_prev (WDGT_RING **current)
{
    WDGT_RING *new = (*current)->wr_prev;
    
    /* Use x->wr_label to determine if it's the form/menu, etc */
    /* If it doesn't have a label, it doesn't need reprinting  */
    /* I'll probably add a field for this later */
    
    if ((*current)->wr_label)
    {
        wr_print_button (*current, FALSE);
    }

    if (new->wr_label)
    {
        wr_print_button (new, TRUE);
    }

    *current = new;
    return new;
}

/* ********************************************** *
 * widget_ring_next() - updates window to go to   *
 *    next widget in ring                         *
 * Passed: WINDOW in which widgets are            *
 *         WDGT_RING in which widget resides      *
 *         current widget                         *
 * ********************************************** */

WDGT_RING *
widget_ring_next (WDGT_RING **current)
{
    WDGT_RING *new = (*current)->wr_next;
    
    /* Use x->wr_label to determine if it's the form/menu, etc */
    /* If it doesn't have a label, it doesn't need reprinting  */
    /* I'll probably add a field for this later */
    
    if ((*current)->wr_label)
    {
        wr_print_button (*current, FALSE);
    }

    if (new->wr_label)
    {
        wr_print_button (new, TRUE);
    }

    *current = new;
    return new;
}

/* ********************************************* *
 * widget_ring_new() -creates a new widget ring. *
 * Returns: pointer to new ring on success,      *
 *          NULL on failure to allocate memory   *
 * ********************************************* */

WDGT_RING *
widget_ring_new (WINDOW * wndw, int yloc, int xloc, char *label, int retval)
{
    WDGT_RING *new;

    new = (WDGT_RING *)calloc (1, sizeof (WDGT_RING));
    
    if (!new)
    {
        return NULL;
    }

    /* All ptrs point to itself since we have a 1-element "wheel" */
    
    new->wr_first = new->wr_prev = new->wr_next = new;

    new->wr_y = yloc;
    new->wr_x = xloc;
    new->wr_label = label;
    new->wr_retval = retval;
    new->wr_win = wndw;

    return new;
}

/* ************************************************** *
 * widget_ring_append() - append a widget ring to the *
 *    "end" of the ring (previous to the first one    *
 * Passed: ptr to any element in the ring             *
 * Returns: ptr to the new widget element             *
 *          NULL if error                             *
 * ************************************************** */

WDGT_RING *
widget_ring_append (WDGT_RING *ring,
                    int yloc, int xloc, char *label, int retval)
{
    WDGT_RING *begin,
              *new;

    begin = ring->wr_first;
    
    new = (WDGT_RING *)calloc (1, sizeof (WDGT_RING));

    if (!new)
    {
        return NULL;
    }

    /* Now set up the ring pointers, inserting
     * the new widget into the ring */
    
    new->wr_first = begin;    /* Always */
    new->wr_next = begin;
    new->wr_prev = begin->wr_prev;
    
    (begin->wr_prev)->wr_next = new;
    begin->wr_prev = new;

    new->wr_y = yloc;
    new->wr_x = xloc;
    new->wr_label = label;
    new->wr_retval = retval;
    new->wr_win = begin->wr_win;

    return new;
}

/* ********************************************* *
 * widget_ring_free() - deallocate a whole ring  *
 * Passed: ptr to any element in the ring        *
 * ********************************************* */

void
widget_ring_free (WDGT_RING *ring)
{
    WDGT_RING *next, *this;

    if (!ring)   /* if pointer is NULL, just return */
    {
        return;
    }
    
    this = ring;  /* it doesn't matter where we start, it's a closed loop */

    /* In case there's only a single element */
    
    if (this->wr_prev == this->wr_next)
    {
        free (this);
        return;
    }
    
    next = this->wr_next;
    (this->wr_prev)->wr_next = NULL;  /* make an "end" to the list */
    free (this);

    while ((this = next))
    {
        next = this->wr_next;
        free (this);
    }
}

/* ********************************************** *
 * wr_print_button() - prints out a button at the *
 *       prescribed location, highlighted if it's *
 *       the currently-selected ring              *
 * Passed: window to write to, current ring,      *
 *                             selected ring      *
 * ********************************************** */

void
wr_print_button (WDGT_RING *current, bool is_hot)
{
    chtype attrsave;

    
    attrsave = getattrs (current->wr_win);

    if (is_hot)
    {
        wattrset (current->wr_win, BUT_SEL);
    }
    else
    {
        wattrset (current->wr_win, BUT_UNSEL);
    }

    mvwaddch (current->wr_win, current->wr_y, current->wr_x, '[');
    wprintw (current->wr_win, current->wr_label);
    waddch (current->wr_win, ']');

    wattrset (current->wr_win, attrsave);
}

/* ************************************************** *
 * wr_button_new() - creates a button widget and adds *
 *    it to an existing widget ring                   *
 * Passed: WINDOW *win (window it's to be written in  *
 *         WDT_RING * (does wr-new() if NULL          *
 *         y position in that window,                 *
 *         x position "   "     "                     *
 *         pointer to the label for the button        *
 *         return value for that button               *
 * Returns: WDGT_RING * on success, NULL on failure   *
 * ************************************************** */

WDGT_RING *
wr_button_new (WINDOW *win, WDGT_RING *ring,
               int xpos, int ypos, char *label, int retval)
{
    WDGT_RING *newring;
    
    if (!ring)
    {
        newring = widget_ring_new (win, xpos, ypos, label, retval);
    }
    else
    {
        newring = widget_ring_append (ring, xpos, ypos, label, retval);
    }

    if (!newring)
    {
        return NULL;
    }

    wr_print_button (newring, FALSE);

    return newring;
}

