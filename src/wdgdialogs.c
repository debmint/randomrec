/* ************************************************************ *
 * wdgdialogs.c - various dialogs created for specific purposes *
 * ************************************************************ */

/* $Id: wdgdialogs.c,v 1.1 2007/01/16 23:55:32 dlb Exp $ */

/* NOTE:  This file is #included by curslib.c */

enum {
    df_yr,
    df_mo,
    df_da,
    NDATE_ENTRIES
};

#include <time.h>
#include <ctype.h>
/*#include <form.h>*/

/* ********************************************************* *
 * strip_whites () - This strips off all leading & trailing  *
 *    whitespaces (space or tab) from a string               *
 * Passed : the address of the string to strip               *
 * Returned: The same address - the string is moved up to    *
 *        the start if leading spaces are encountered        *
 * ********************************************************* */

char *
strip_whites (char *str)
{
    char *here,
         *to;
    
    /* Strip end */

    here = &str[strlen (str) - 1];
    
    while ((*here == ' ') || (*here == '\t'))
    {
        --here;
    }

    ++here;
    *here = '\0';

    /* Trailing blanks are taken care of. if no leading blanks, we're done */
    
    if ((*str != ' ') && (*str != '\t'))
    {
        return str;
    }
    else {
    
        here = str;

        while ((*here == ' ') || (*here == '\t'))
        {
            ++here;
        }

        to = str;

        /* Now move characters back to the begin of string */
        /* we need to move at least one (even if it's null */
        
        while (*here)
        {
            *(to++) = *(here++);
        }

        /* copy the last null byte */
        *to = *here;
            
    }

    return str;
}

/* **************************************************** *
 * strncpyfld () - copies n bytes from a field, nulls   *
 *         strips the string, and insures a null byte   *
 *         is at the end                                *
 * Passed : similar to strncpy except that a FIELD *    *
 *         is the source (will convert to field_buffer  *
 *         internally.                                  *
 * Returns: address of destination string               *
 * NOTES:  The user must ensure enough space in dest    *
 * **************************************************** */

char *
strncpyfld (char *dst, FIELD *field, int count)
{
    /* copy count - 1 because we must be sure of a trailing NULL */
    strncpy (dst, field_buffer (field, 0), count-1);
    dst[count - 1] = '\0';
    
    strip_whites (dst);
    return dst;
}

/* ********************************************************* *
 * wdg_input_box() - Create a window and accept input for    *
 *     a string.                                             *
 * Passed : A Prompt for the box                             *
 *          The max size for the requested string            *
 * Returned: Null on "Cancel"  or a pointer to a duped       *
 *           string that was input.  This string is STRIPPED *
 *           of leading/trailing whitespaces                 *
 * NOTE!!!   This string should be free() 'd after returning *
 *           from this routine                               *
 * ********************************************************* */

char *
wdg_input_box (char *pmpt, int entsize)
{
    FIELD *entry[2];  /* Single field + NULL */
    FORM  *form;
    WINDOW *iWin;

    int winH,
        winW;
    
    char tell[] = "<F1> to Save   <F5> to Cancel";
    char *retstr = NULL;   /* Default to NULL ptr */
    int ch;
    bool done = FALSE;

    winW = strlen (pmpt) + 2;
    
    if ( entsize > winW)
    {
        winW = entsize;
    }

    if (winW < strlen (tell))
    {
        winW = strlen (tell);
    }
    
    winW += 4;

    if (winW > COLS)
    {
        winW = COLS;
    }
    
    winH = 7;  /* Just assume a fixed height for now */
    
    /* Set up the Form */
    
    entry[0] = new_field (1, entsize, 0, 0, 0, 0);
    entry[1] = NULL;

    form = new_form (entry);
    
    iWin = newwin (winH, winW, (LINES - winH)/2, (COLS - winW)/2);
    keypad (iWin, TRUE);
    wbkgdset (iWin, COLOR_SET (COLOR_WHITE, COLOR_BLUE));
    werase (iWin);
    wattron (iWin, A_BOLD);
    box (iWin, 0, 0);
    wattroff (iWin, A_BOLD);
    
    set_form_win (form, iWin);
    set_form_sub (form, derwin (iWin,
                                1, entsize, winH - 4, (winW - entsize)/2));
    
    wattron (iWin, A_REVERSE);
    mvwprintw (iWin, 1, (winW - (strlen (pmpt) + 2))/2, " %s ", pmpt);
    mvwprintw (iWin, winH - 2, (winW - strlen (tell))/2, tell);
    wattroff (iWin, A_REVERSE);
    wrefresh (iWin);

    post_form (form);

    do {
        done = FALSE;
        
        switch ((ch = wgetch (iWin)))
        {
            case '\x0a':
            case '\x0d':
                break;    /* Ignore for now */
            case KEY_F(5):  /* Cancel */
                done = TRUE;
                retstr = NULL;
                break;
            case KEY_F(1):   /* Accept new entry */
                form_driver (form, REQ_END_LINE);
                retstr = strdup (field_buffer (entry[0], 0));

                /* Go strip off leading/trailing whitespaces */
                strip_whites (retstr);
                
                if (strlen (retstr) >= entsize)
                {
                    retstr[entsize - 1] = '\0';
                }
                
                if (!strlen (retstr) || !strcmp (retstr, " "))
                {
                    free (retstr);
                    retstr = NULL;
                }
                
                done = TRUE;
                break;
            case KEY_BACKSPACE:
                form_driver (form, REQ_DEL_PREV);
                form_driver (form, REQ_END_LINE);
                break;
            case KEY_DC:
                form_driver (form, REQ_DEL_CHAR);
                form_driver (form, REQ_END_LINE);
                break;
            default:
                form_driver (form, ch);
                break;
        }     /* End switch (ch=wgetch(iWin) */
    } while (!done);

    /* clean up */
    
    unpost_form (form);
    free_form(form);
    free_field(entry[0]);

    delwin (iWin);

    return retstr;
}

static int
date_init (int cur_time, int src, int min, int max)
{
    if (src && (min <= src) && (src <= max))
    {
        return src;
    }
    else
    {
        return cur_time;
    }
}

/* ************************************************************* *
 * wdg_get_date () get a date                                    *
 * Passed:  WINDOW * to parent window (resets window if not NULL *
 *          ptr to string in which to store date strng on return *
 *      NOTE: the returned year string is in 2-digit format      *
 *           it's left up to the application to convert it if    *
 *           needed                                              *
 * ************************************************************* */

void
wdg_get_date (WINDOW *win, int dyr, int dmo, int dday, char *ret_str)
{
    FIELD *field[NDATE_ENTRIES + 1];
    FORM *my_form;
    WINDOW *date_win,
           *sbf_win;

    WDGT_RING *ring,
              *current;
    
    time_t curtime;
    struct tm *lcltime;

    int dates[3] = { 0, 0, 0 };

    int idx = 0;
    int a, c;
    char cbuf[10];

    int date_max[] = { 3000,
                        12,
                        31
    };
    
    time (&curtime);
    lcltime = localtime (&curtime);

    dates[0] = date_init ((lcltime->tm_year) % 100, dyr, 0, 3000);
    dates[1] = date_init (lcltime->tm_mon, dmo, 1, 12);
    /* TODO fix months better */
    dates[2] = date_init (lcltime->tm_mday, dday, 1, 31);

    c = 4;
    
    for (a = 0; a < NDATE_ENTRIES; a++)
    {
        int *mpt = date_max;
        int mmin = 0;

        field[a] = new_field(1, 2, 1, c, 0, 0);
        c += 10;
        set_field_back (field[a], COLOR_SET (COLOR_BLACK, COLOR_WHITE));
        set_field_type (field[a], TYPE_INTEGER, 2, mmin, mpt++);
        mmin = 1;
    }

    field[NDATE_ENTRIES] = NULL;
    
    my_form = new_form (field);
    
    date_win = newwin (10, 30, LINES/2 - 5, COLS/2 - 15);
    keypad (date_win, TRUE);
    wattrset (date_win, COLOR_SET (COLOR_BLACK, COLOR_YELLOW));
    wbkgdset (date_win, COLOR_SET (COLOR_BLACK, COLOR_YELLOW));
    werase (date_win);
    box (date_win, 0, 0);

    set_form_win (my_form, date_win);
    set_form_sub (my_form, (sbf_win = derwin (date_win, 2, 28, 4, 1)));

    wattron (date_win, A_BOLD);
    mvwprintw (date_win, 1, 10, "Date Entry");
    wattroff (date_win, A_BOLD);
    mvwprintw (date_win, 3,  4, "Yr:");
    mvwprintw (date_win, 3, 14, "Mo:");
    mvwprintw (date_win, 3, 24, "Da:");

    ring = widget_ring_new (date_win, 2, 2, NULL, 0);
    current = ring;
    
    mvwaddch (date_win, 7, 0, ACS_LTEE);
    whline (date_win, 0, 28);
    mvwaddch (date_win, 7, 29, ACS_RTEE);
    
    wr_button_new (date_win, ring, 8,  3, "Accept", 1);
    wr_button_new (date_win, ring, 8, 14, "Cancel", -1);
   
    /* Now intialize the forms with the suppled dates */
    
    for (c = 0; c < NDATE_ENTRIES; c++)
    {
        sprintf (cbuf, "%02d", dates[c]);
        set_field_buffer (field[c], 0, cbuf);
    }
    
    post_form (my_form);
    wrefresh (date_win);

    while (1)
    {
        bool done = FALSE;

        switch (c = wgetch (date_win))
        {
            /* Emergency bailout */
            case KEY_F(1):
                done = TRUE;
                break;
            case 0x0a:
            case 0x0d:
                if (current == ring)
                {
                    form_driver(my_form, REQ_NEXT_FIELD);
                    form_driver(my_form, REQ_END_LINE);
                    /*wrefresh (date_win);*/
                }
                else
                {
                    form_driver(my_form, REQ_END_LINE); /* Clean up */
                    done = TRUE;
                }

                break;
            case 9: /* TAB */
                idx = field_index (current_field (my_form));
                sscanf (field_buffer (field[idx], 0), "%d", &dates[idx]);
                widget_ring_next (&current);
                break;
            case KEY_UP:
                if (current == ring)
                {
                    idx = field_index (current_field (my_form));
                    sscanf (field_buffer (field[idx], 0), "%d", &dates[idx]);
                    ++dates[idx];
                    sprintf(cbuf, "%2.2d", dates[idx]);
                    set_field_buffer (field[idx], 0, cbuf);
                }
                break;
            case KEY_DOWN:
                if (current == ring)
                {
                    idx = field_index (current_field (my_form));
                    sscanf (field_buffer (field[idx], 0), "%d", &dates[idx]);
                    --dates[idx];
                    sprintf(cbuf, "%2.2d", dates[idx]);
                    set_field_buffer (field[idx], 0, cbuf);
                }
                break;
            case KEY_RIGHT:
                if (current == ring)
                {
                    form_driver(my_form, REQ_NEXT_FIELD);
                    form_driver(my_form, REQ_END_LINE);
                }
                break;
            case KEY_LEFT:
                if (current == ring)
                {
                    form_driver(my_form, REQ_PREV_FIELD);
                    form_driver(my_form, REQ_END_LINE);
                }
                else {
                    do {
                        current = widget_ring_prev (&current);
                    } while (!current->wr_label);
                }
                break;
            case KEY_BACKSPACE:
                if (current == ring)
                {
                    form_driver (my_form, REQ_DEL_PREV);
                    form_driver (my_form, REQ_END_LINE);
                }
                else {
                    do {
                        widget_ring_prev (&current);
                    } while (!current->wr_label);
                }
                break;
            case KEY_DC:
                if (current == ring)
                {
                    form_driver(my_form, REQ_DEL_CHAR);
                }
                break;
            default:
                form_driver(my_form, c);
                idx = field_index (current_field (my_form));
                sscanf (field_buffer (field[idx], 0), "%d", &dates[idx]);
        }

        if (done)
        {
            /* Dunno why this is needed, but you don't get the last
             * character typed if you don't have it??? */
            form_driver(my_form, REQ_END_LINE);

            if (current->wr_retval == -1)
            {
                break;    /* Cancel - don't do anything */
            }
            else
            {
                sprintf (ret_str, "%s/%s/%s", field_buffer (field[0], 0),
                                               field_buffer (field[1], 0),
                                               field_buffer (field[2], 0));
                /**dyr = atoi (field_buffer (field[0], 0)); 
                *dmo = atoi (field_buffer (field[1], 0));
                *dday = atoi (field_buffer (field[2], 0));*/
                
            }
            break;
        }
    }

    unpost_form (my_form);
    free_form(my_form);
    free_field(field[0]);
    free_field(field[1]);
    free_field(field[2]);

    delwin (date_win);
    
    if (win)
    {
        wrefresh (win);
    }
}
