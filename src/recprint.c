
/* recprint.c   printout routines ... for either screen or printer */

#include "record.h"
#include <ctype.h>

void PutCode(FILE *, char *);
char *skipwhite(char *);


static bool WantInc;

static int LinNum,
           PagNum,
           PagLen;

char *mo_nam[13] = {   "",    /* Start with null so Jan is mo_nam[1] */
	"Jan.", "Feb.", "March",
	"April", "May", "June",
	"July", "Aug.", "Sept.",
	"Oct.", "Nov.", "Dec."
};

char *itm_fmt =".T&\nl | l | l | l | l | l | n.";
char *itm_ttl_fmt =".T&\nl   l   l   l   l   l   n.";

static FILE *Pr_ON(void)
{
    char *otype;
    char *odev;
    char pipecmd[300];

    if (isprntr) {
        otype = "ps";
        odev = "lpr";
    }
    else {
        otype = "ascii";
        odev = "less";
        
        /* Exit from curses for screen printout */
        def_prog_mode();    /* save current tty modes */
        endwin();           /* restore original tty modes */
    }

    snprintf (pipecmd, sizeof(pipecmd), "groff -t -ms -T%s | %s\n", otype, odev);
    
	return popen (pipecmd,"w");
}

static int Pr_OFF (FILE *ppath)
{
    int result;
    result = pclose (ppath);

    /* If !isprntr, but a refresh() shouldn't hurt, should it? */
    if (!isprntr)
    {
        refresh();
    }

    return result;
}

/* ********************************** *
 * itmized_hdr() - sets up the header *
 * for an itemized list               *
 *                                    *
 * Returns with headers printed and   *
 *   tbl set up for itm_ttl_fmt ready *
 *   to print a cgy-name line         *
 * ********************************** */

static void itmized_hdr (FILE *fp, char *jobname, char *rept_typ)
{
   if ((fprintf (fp, ".tl '\\n(mo/\\n(dy/\\n[year]'%s Itemized Account'\n",
                     jobname))<0)
   {
       printw("Error in writing to pipe\n");
   }

   fputs (".TS H\nexpand;\nl c s s\ncb cb cb cb cb cb cb.\n", fp);
   fprintf (fp, "\\n(mo/\\n(dy/\\n[year]\t%s %s\n", jobname, rept_typ);
   fputs ("Date\tDescription\tQuan\tInvc\tChk\tTo/From Whom\tAmount\n", fp);

   /* end header define, print a blank line, and
    * set up to print cgy-name line
    */
   
   fprintf (fp, "_\n.TH\n%s\n", itm_ttl_fmt);
}

      /* ******************************************* *
       * PrtCgy(mainttl,subttl,cgynam,cgynum,cgyttl) *
       * : print transactions for one cagegory       *
       * ******************************************* */

static int PrtCgy(FILE *outp, char *cgyname,int cgnum,double cgyttl)
{

	register int curmo=0;

    rewind (fpath);
    
    /* Print the Category Name */
    fprintf(outp,"\n\t%s\n%s\n", cgyname, itm_fmt);

	while ( fread(&Rec,sizeof(Rec),1,fpath) == 1 ) {
		if ( (Rec.Cgy == cgnum) && !(WantInc^Rec.IsIncom) && Rec.Cost) {
			if( (int)Rec.Date.Mo > curmo ) {
				if(curmo) {
					fputs ("\n", outp);
				}

				curmo = (int)Rec.Date.Mo;
			}
			
                     /* Now we can print the transaction */
			fprintf (outp, "%02d/%02d/%02d\t%s\t%s\t%s\t%s\t%s\t%9.2f\n",
                           Rec.Date.Mo, Rec.Date.Da, Rec.Date.Yr%100,
                           Rec.Descr, Rec.Quan, Rec.Invoic, Rec.ChkNo,
                           Person[(int)Rec.ToFrm].Who, Rec.Cost);
		}
	}
    
    /* Now print out totals for CGY */
    
    /* Change table format for ttls */
    fprintf (outp, "_\n%s\n", itm_ttl_fmt);
	fprintf (outp, "\tTotal for %s\t\t\t\t\t$%9.2f\n",cgyname,cgyttl);
    /* restore listing format for (possible) next cgy */
    fprintf (outp, "%s\n", itm_fmt);

                /* We got here through an "fread()" error. */
	if ( !feof(fpath) )		/* If we're not at end-of-file */
		return ERR;		/*  then it's a true error     */
	clearerr (fpath);		/* EOF not a real error        */
	return 0;
}

/* PrEntir() : print Entire Transactions file */

static int
PrEntir(void)
{
    char pttl[80];
    register char *Hdr;
    register int srchcgy;
    FILE *ps_file = 0;

	LinNum = 1;

	sprintf(pttl, "%d %s Itemized Account", 1900+RecYr, jobptr->JobNam);
    Hdr = "* E x p e n s e s * ";
    WantInc = FALSE;
   
    if( !(fpath=fopen(datafil,"r")) ) {
        opn_err(tmpstr);
        return(ERR);
    }

    if (YrTtl.TotXpns)
    {
        if (!(ps_file = Pr_ON()))
        {
            fclose (fpath);
            printw("Error in opening output path\n");
            opn_err("Print output path");
            return ERR;
        }
 
        /* ****************************** *
         * Now set up the EXPENSE groff    *
         * ****************************** */
 
        itmized_hdr (ps_file, jobptr->JobNam, "Expense");
    
        for (srchcgy = 0; srchcgy < ExCgyNum; srchcgy++ ) {
           if (Expense[srchcgy].CgyTot ) {
              PrtCgy (ps_file, Expense[srchcgy].CgyNam, srchcgy,
                               Expense[srchcgy].CgyTot);
           }
        }
 
        fputs ( ".TE\n", ps_file);
        Pr_OFF (ps_file);
        ps_file = 0;
    }
    
                      /* Now do Incomes */
    if (YrTtl.TotInc)
    {
        if (!(ps_file = Pr_ON()))
        {
            fclose (fpath);
            printw("Error in opening Incomes pipe\n");
            opn_err("Print output path");
            return ERR;
        }
 
        /* ****************************** *
         * Now set up the INCOME groff    *
         * ****************************** */
 
        itmized_hdr (ps_file, jobptr->JobNam,"Income");
   

			/*  print entire file, one CGY at a time */

        WantInc = TRUE;
      
        for (srchcgy = 0; srchcgy < InCgyNum; srchcgy++ ) {
            if (Income[srchcgy].CgyTot ) {
                PrtCgy (ps_file, Income[srchcgy].CgyNam, srchcgy,
                                 Income[srchcgy].CgyTot);
            }
        }

        fputs ( ".TE\n", ps_file);
        Pr_OFF (ps_file);
    }

    return 0;
}

/* ********************************** *
 * OneCgy() - choose a single         *
 * category to do an itemized listing *
 * ********************************** */

static int
OneCgy(void)
{
	register int cgnum;
	register struct cgystr *cgyptr;
    FILE *ps_file = 0;

   cgyshow();
   CurXY(scrn,0,22);
   printw("Choose a category... ");
   refresh();
   
   do {
      CurXY(scrn,24,22);ErEOLine(scrn);
      getnstr(tmpstr,sizeof(tmpstr));
      cgnum = atoi(tmpstr);
   } while ( !cgnum || (cgnum > InCgyNum+ExCgyNum) );

   --cgnum;     /* start with # 0 */

   if (cgnum >= InCgyNum)
   {
      cgnum-=InCgyNum;
      WantInc = FALSE;
      cgyptr = &Expense[cgnum];
   }
   else {
      WantInc = TRUE;
      cgyptr = &Income[cgnum];
   }
   
    /* If no transactions for this cgy, don't process */
    /* TODO : issue a warning in this case */
   
    if (cgyptr->CgyTot == 0)
    {
        return ERR;
    }
    
    if ( !(fpath=fopen(datafil,"r")) )
    {
		opn_err (tmpstr);
		return (ERR);
	}

   if (!(ps_file = Pr_ON()))
   {
       fclose (fpath);
       printw("Error in opening output path\n");
       opn_err("Print output path");
       return ERR;
   }

    itmized_hdr (ps_file, jobptr->JobNam, WantInc ? "Income" : "Expense");
	PrtCgy (ps_file, cgyptr->CgyNam, cgnum, cgyptr->CgyTot);
            /*(WantInc ?Income[cgnum].CgyTot : Expense[cgnum].CgyTot) );*/

    if (ps_file)
    {
       fputs ( ".TE\n", ps_file);
        Pr_OFF (ps_file);
    }

	return 0;
}

static void
Gen_Sum(void)
{
	register int count;
    FILE *ps_file = 0;

    if (!(ps_file=Pr_ON()))
    {
        printw("Error in creating pipe\n");
       /* return ERR;*/
        return;
    }

    /* Print title and headers */
    fprintf (ps_file,
           ".tl 'SUMMARY of INCOME/EXPENSE for %s''\\n(mo/\\n(dy/\\n[year]'\n",
           jobptr->JobNam);
    fputs (".TS H\nexpand;\nc c   c c.\n", ps_file);
    fputs ("Income\tAmount\tExpense\tAmount\n_\n.TH\n",ps_file);
    fputs (".T&\nl n   l n.\n",ps_file);

	for ( count=0; count<InCgyNum || count<ExCgyNum; count++ ) {
        if ( count<InCgyNum  ) {
            fprintf(ps_file, "%s\t%5.2f", Income[count].CgyNam,
                                          Income[count].CgyTot);
        }
        else {
            fprintf(ps_file,"\t");
        }

        if ( count<ExCgyNum  ) {
            fprintf (ps_file, "\t%s\t%5.2f", Expense[count].CgyNam,
                                          Expense[count].CgyTot);
        }
        
        fputs ("\n",ps_file);
    }
   
    fputs ("_\n",ps_file);
    fprintf (ps_file, "%s\t%5.2f\t%s\t%5.2f\n",
                      "Total Income...", YrTtl.TotInc,
                      "Total Expense..", YrTtl.TotXpns);
    fprintf (ps_file, "\t\t%s\t%5.2f\n", "Net Profit/Loss", 
                                         YrTtl.TotInc-YrTtl.TotXpns);

    if (ps_file)
    {
       fputs ( ".TE\n", ps_file);
        Pr_OFF (ps_file);
    }

}

static int
pp_summ(void)
{
	register int  count;
    FILE *ps_file;

    if (!(ps_file=Pr_ON()))
    {
        printw("Error in creating pipe\n");
       /* return ERR;*/
        return ERR;
    }

	fprintf(ps_file, ".tl 'PAYOR/PAYEE SUMMARY for %s''as of %d/%d/%d\n",
				     jobptr->JobNam, YrTtl.LstUpdat.Mo,
                                     YrTtl.LstUpdat.Da,
                                     YrTtl.LstUpdat.Yr);
    fputs (".TS H\nc c c.\nName\tRcv'd From\tPaid To\n_\n.TH\n", ps_file);
    fputs (".T&\nl n n.\n", ps_file);
    
	for ( count=0; count<NumPayrs; count++ ) {
		fprintf(ps_file, "%s\t%9.2f\t%9.2f\n", Person[count].Who,
		                 Person[count].GotFrom,Person[count].PdTo);
	}
	
    if (ps_file)
    {
        fputs ("_\n.TE\n", ps_file);
        Pr_OFF (ps_file);
    }

    return 0;
}

	/*  Mo_Summ():  computes a monthly summary of incomes  */

int Mo_Summ(void)
{
	double	mo_inc[13], mo_exp[13],
		in_tot, xp_tot;
	register int ct;
	register int mo_stop;
    FILE *ps_file;

    if (!(ps_file=Pr_ON()))
    {
        printw("Error in creating pipe\n");
       /* return ERR;*/
        return ERR;
    }

	for( ct=0; ct < 13; ct++ )
		mo_inc[ct] = mo_exp[ct] = 0;

	if( !(fpath=fopen(datafil,"r")) ) {
		opn_err(tmpstr);
		return(ERR);
	}

    rewind (fpath);

	while ( fread(&Rec,sizeof(Rec),1,fpath) == 1 ) {
		if( Rec.IsIncom) {
			mo_inc[Rec.Date.Mo] += Rec.Cost;
		}
		else {
			mo_exp[Rec.Date.Mo] += Rec.Cost;
		}
	}
	fclose( fpath );

	mo_stop = 12;			/* default to print all 12 months */
	if ( RecYr == Today.Yr)  /* but if this year, */
		mo_stop = Today.Mo;	/* stop with this month  */
	
	in_tot = xp_tot = 0;

    /* Print title and headers */
    fprintf (ps_file,
           ".tl 'MONTHLY Income/Expense  SUMMARY for %s''\\n(mo/\\n(dy/\\n[year]'\n",
           jobptr->JobNam);
    
	sprintf(tmpstr,"for record year %d", 1900+RecYr);

    fputs (".TS H\nexpand;\nc c c | c c.\n", ps_file);
    fputs ("Month\tReceipts\tTtl Rcpts\tExpenses\tTot Xps\n_\n.TH\n", ps_file);
    fputs (".T&\nl n n | n n.\n", ps_file);

	for( ct=1; ct <= mo_stop; ct++ ) {
		fprintf(ps_file, "%s\t%5.2f\t%5.2f\t%10.2f\t%10.2f\n",
			             mo_nam[ct],
			             mo_inc[ct], in_tot+=mo_inc[ct],
			             mo_exp[ct], xp_tot+=mo_exp[ct] );
	}

    if (ps_file)
    {
        fputs ("_\n.TE\n_\n", ps_file);
        Pr_OFF (ps_file);
    }

	return 0;
}


			/* Main menu loop begins here */

int recprint(void)
{

        /* Set up defaults, depending on whether o/p is to
         * screen or printer */

    clear();
    refresh();

    if (!printwin)
    {
        curses_create_print_window ();
        printwin = dupwin (stdscr);
    }
   
    wattrset (printwin, A_STANDOUT | COLOR_PAIR (COLOR_WHITE));
    
    if (isprntr)
    {
        mvwprintw (printwin, 0, 20,
                   " H A R D C O P Y    P R I N T   O P T I O N S ");
        PagLen = 66;
    }
    else
    {
        mvwprintw (printwin, 0, 20,
                   " S C R E E N    P R I N T   O P T I O N S ");
        PagLen = 24;
    }

    wattrset (printwin, A_NORMAL);
    
/* ****************************************************** *
 *                  Main Menu Loop                        *
 * ****************************************************** */
	for ( ;; )
    {
        wmove (printwin, 10, 50);
        redrawwin (printwin);
        wrefresh (printwin);
        
        LinNum=1; PagNum=1;
		
        switch (getch()) {
			case '1':
				PrEntir();
				break;
			case '2':
				OneCgy();
				break;
			case '3':
				Gen_Sum();
				break;
			case '4':
				pp_summ();
				break;
			case '5':
				Mo_Summ();
				break;
			case '6':	/*   Quit, return to main menu   */
				return 0;
			default:
				break;
		}
	}
}

char *skipwhite(char *p)
{
	while(isspace(*p))
		++p;
	
	return p;
}
