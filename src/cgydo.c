/* ******************************************************** *
 * Cgy edit functions:   Display cagegories, add categories,*
 * warn if max # has been reached                           *
 * ******************************************************** */

/* $Id$ */

#include "record.h"

#include <ctype.h>

#ifdef CURSES
#define printf printw
#endif

/* ******************************************** *
 * cgyshow_print_cgy() - prints a category in   *
 *    its position in a 3-column list           *
 * Passed: the number, &ypos                    *
 * Returns: yloc used . can be used to know     *
 *     the highest value y reaches              *
 * ******************************************** */

static int
cgyshow_print_cgy (int count, int yloc)
{
    int mycount = count,
        cgsize,
        ymax;
    struct cgystr *cgypt;

    if (count >=InCgyNum)
    {
        mycount -= InCgyNum;
        cgsize = ExCgyNum;
        cgypt = &Expense[mycount];
    }
    else
    {
        cgsize = InCgyNum;
        cgypt = &Income[mycount];
    }

    ymax = (mycount % ((cgsize + 2) / 3)) + yloc;
    move (ymax, 26 * (mycount / ((cgsize + 2) / 3)));
    attrset (COLOR_PAIR (COLOR_GREEN) | A_BOLD);
    printw ("%2d", count+1);
    attrset (A_NORMAL);
    printw ("-%-22s", cgypt->CgyNam);
    
    return ymax;
}


void
cgyshow (void)
{
    register int count;         /* for.. next counter */
    int ypos, ymax;

    clear();
    attrset (COLOR_PAIR (COLOR_WHITE) | A_STANDOUT);
    mvprintw (0, 15, " I N C O M E / E X P E N S E   C A T E G O R I E S ");
    attrset (A_NORMAL);
    
    /*  Columnize Income Classes   */
    attrset (COLOR_PAIR (COLOR_CYAN) | A_UNDERLINE);
    mvprintw (1, 0, "Income Classes");
    attrset (A_NORMAL);
    
    ypos = 2;
    ymax = ypos;

    for (count = 0; count < InCgyNum; count++)
    {
        int yhit;
        if ((yhit = cgyshow_print_cgy (count, ypos)) > ymax)
        {
            ymax = yhit;
        }
    }
    
    /*  Columnize Expense Classes   */
    /*CurXY (scrn, 0, 8);*/
    ypos = ymax + 2;
    attrset (COLOR_PAIR (COLOR_CYAN) | A_UNDERLINE);
    mvprintw (ypos, 0, "Expense Classes");
    attrset (A_NORMAL);
   
    ++ypos;
    
    for (count = 0; count < ExCgyNum; count++)
    {
        /*CurXY (scrn, 26 * (count / ((ExCgyNum + 2) / 3)),
               (count % ((ExCgyNum + 2) / 3)) + 9);
        printf ("%2d-%-22s", InCgyNum + count + 1, Expense[count].CgyNam);*/
        cgyshow_print_cgy (count+InCgyNum, ypos);
    }
    
    refresh ();
}

/* ******************************************* *
 * addcgy () : add an income/expense category  *
 * Returns: 1 on success, 0 on non-fatal error *
 *          -1 on fatal error                  *
 * ******************************************* */

int
addcgy (void)
    /* Add a new category to file    */
{
    char inpstr[sizeof (Income[1].CgyNam) + 1];
    char *wstr = "Didn't Work!";
    register int *cgysiz = cgysiz = &ExCgyNum,
                 count;
    register struct cgystr *tmpcgy = Expense;

    cgyshow ();
    printf ("\n\n");

    printf ("0=quit/done   I=add INCOME cgy    X=add EXPENSE cgy >>  ");
    
    do
    {
        fflush (stdout);

        inpstr[0] = (char) getch ();

    }
    while (!strchr ("0IiXx", inpstr[0]));
    printf ("\n");

    /* First set up for add */
    switch (inpstr[0])
    {
    case '0':
        return (0);
    case 'I':
    case 'i':
        wstr = "Income";
        tmpcgy = Income;
        cgysiz = &InCgyNum;
        
        if (InCgyNum >= sizeof (Income) / sizeof (Income[1]))
        {
            cgyfull (wstr);
            return (0);
        }
        
        break;
    case 'X':
    case 'x':
        wstr = "Expense";
        tmpcgy = Expense;
        cgysiz = &ExCgyNum;
        
        if (ExCgyNum >= sizeof (Expense) / sizeof (Expense[1]))
        {
            cgyfull (wstr);
            return (0);
        }
        
        break;
    }
    /* we are now set to go on and add one */
    printf ("Add %s category... \n[ENTER] alone to abort...", wstr);
    do
    {
#ifdef CURSES
        getnstr (inpstr, sizeof (inpstr));
#else
        n_gets (inpstr, sizeof (inpstr));
#endif
        if (strlen (inpstr) >= sizeof (Income[1].CgyNam))
        {
            printf ("Too long.. Limit to %d chars\n",
                    sizeof (Income[1].CgyNam - 1));
        }
    }
    while (strlen (inpstr) >= sizeof (Income[1].CgyNam));
    if (!strlen (inpstr))
    {
        return (0);
    }
    /* Now check to see if this name matches any already there */
    count = 0;
    while (count++ < *cgysiz)
    {
        if (!strcasecmp (inpstr, (tmpcgy++)->CgyNam))
        {
            printf ("%s is already recorded.  Aborting...\n Press [ENTER]..",
                    inpstr);
            inpstr[0] = '\0';   /* Null string to flag match */
            getch ();
            return (0);
        }
    }
    CgyDflts (tmpcgy);

    /* System all set to save new entry... Verify you really want to */
    /* Note: at this point, tmpcgy is pointing to the first empty
       spot in the Catetory array                                    */
    cls;
    printf ("Add ");
    ReVOn (STDOUT);
    printf (" %s ", inpstr);
    ReVOff (STDOUT);
    printf (" to... %s Are you sure <Y/N> ", wstr);
    ans = yesno ();
    if (ans != 'Y')
    {
        inpstr[0] = '\0';
        return (0);
    }
    else
    {
        ++*cgysiz;              /* Bump size of category (whichever it is) */
        strcpy (tmpcgy->CgyNam, inpstr);
        tmpcgy->CgyTot = 0;

        /* *********************************************** *
         * Now, if we are in "record", save it because     *
         * it is probably a one-shot deal.  If in recinit, *
         * wait, write whole file at one time              *
         * *********************************************** */
        
        /* make either "income[s]." or "expenses[s]."   */
        fwrt (strcpy (inpstr,
                      !strcmp (wstr, "Income") ? incmfil : expfil),
                      tmpcgy->CgyNam, sizeof (Income[0]));
        return (1);        /* ... return 1 to flag success   */
    }
}                               /* End of addcgy */

void
cgyfull (char *wstr)
{
    printw ("The %s Category list is full...\n", wstr);
    printw ("Aborting add process.  Press [ENTER]\n");
    getch ();
}

static char *
toggle (char k)
{
    return (k ? "Do" : "Bypass");
}

int
CgyDflts (register struct cgystr *mycgy)
{
    ITEM **py_item;
    MENU *py_menu;
    WINDOW *MnuWin;
    WDGT_RING *ring,
              *cur_ring;
    int mCols,
        mIdx;
    
    char b,
         *des;
    int chflg = 0;

    do
    {
        cls;
        printf ("Editing Category: %s\n\n", mycgy->CgyNam);
        printf ("This affects initial input of data for this field\n\n");
        printf ("These defaults can be overridden in the EDIT phase of ");
        printf ("data input\n");

        if (mycgy->DscDf[0])
        {
            printf ("\nCurrent Description Default: %s\n\n", mycgy->DscDf);
            printf ("E - \"Clear\" Description default\n");
        }

        printf ("\nD - Set new Description default\n");
        printf ("Q - \"%s\" input for Quantity\n", toggle (mycgy->QtyDf));
        printf ("I - \"%s\" input for Invoice\n", toggle (mycgy->InvDf));
        printf ("C - \"%s\" input for Check Number\n", toggle (mycgy->ChkDf));
        if (mycgy->TfDf >= 0)
            printf ("X - \"Bypass\" Input for Payor/Payee\n");
        if (mycgy->TfDf != 0)
            printf ("Y - \"Enable\" Input for Payor/Payee\n");
        printf ("Z - Set default name for Payor/Payee    ");
        if (mycgy->TfDf > 0)
        {
            printf ("Current:  %s\n", Person[mycgy->TfDf - 1].Who);
        }
        printf ("\n\n");
        printf (" 0 - quit\n");
#ifdef CURSES
        b = (char) getch ();
#else
        read (0, &b, 1);
#endif
        if (b > 0x1f)
            b = toupper (b);

        switch (b)
        {
            case 'D':
                /* first param flags write straight to mycgy->DscDf */
                noecho ();
                des = wdg_input_box ("Enter a Description",
                                     sizeof (Rec.Descr));
                echo ();

                if (des)
                {
                    strcpy (mycgy->DscDf, des);
                    free (des);
                }
                else
                {
                    strcpy (mycgy->DscDf, "");
                }
                
                /*ed_dscr (0, mycgy->DscDf);*/
                ++chflg;
                break;
            case 'E':
                mycgy->DscDf[0] = '0';
                ++chflg;
                break;
            case 'Q':
                mycgy->QtyDf = mycgy->QtyDf ^ -1;
                ++chflg;
                break;
            case 'I':
                mycgy->InvDf = mycgy->InvDf ^ -1;
                ++chflg;
                break;
            case 'C':
                mycgy->ChkDf = mycgy->ChkDf ^ -1;
                ++chflg;
                break;
            case 'X':
                mycgy->TfDf = -1;
                ++chflg;
                break;
            case 'Y':
                mycgy->TfDf = 0;
                ++chflg;
                break;
            case 'Z':
                /*mycgy->TfDf = ed_pypr (0);*/  /* 0 flags "no write to record */
                rec_create_menu_tofrom (stdscr, &MnuWin,
                                        &py_item, &py_menu, &mCols, &ring);
                cur_ring = ring;
                wrefresh (MnuWin);
                post_menu (py_menu);
                mIdx = wdg_menu_run(py_menu, &cur_ring, mCols);
                mIdx -= 1; /* Payor # + 1 */

                if ((mIdx != 0) && (mIdx != -2))  /* Either Cancel */
                {
                    if (mIdx != -1 )   /* Ignore "Add Payor" */
                    mycgy->TfDf = mIdx;
                    ++chflg;
                }

                /* Clear out menu */
                rec_clear_menu_tofrom (&MnuWin, &py_menu, &py_item);
                /*redrawwin (stdscr);
                refresh();*/
                break;
            case '0':
                return chflg;
            case '\x1b':
                return chflg;
            default:
                break;
        }
    }
    while (b |= '\x1b');

    return chflg;
}

void
ChDflts (void)
{
    register struct cgystr *mycgy;
    register int inpnum;

    do
    {
        char tmpbuf[10];

        cgyshow ();
        CurXY (scrn, 0, 22);
        printf ("Enter Category # - ESC, ENTER to abort >> ");
        getnstr (tmpbuf, sizeof (tmpbuf));
        inpnum = atoi (tmpbuf);

        if (index (tmpbuf, '\x1b'))
            return;
    }
    while ((inpnum < 1) || inpnum > (InCgyNum + ExCgyNum));

    --inpnum;                   /* Reset ptr to element 0 */
    mycgy =
        (inpnum < InCgyNum) ? &Income[inpnum] : &Expense[inpnum - InCgyNum];

    if (CgyDflts (mycgy))
        UpToDate = FALSE;

}

/* ***************************************************** *
 * ed_cgy () - Display a list of categories to choose    *
 *      from.                                            *
 * Passed : FORM * to edit form (NULL if none)           *
 * Returns: index number of category (Expense            *
 *      concatenated to end of Incomes                   *
 * Redraws base window contained in form_userptr if      *
 *         FORM != NULL                                  *
 * ***************************************************** */

int
ed_cgy (FORM *form)
{
    FIELD *cgy_field;
    char tmpbuf[10];
    register int inpnum;

    cgy_field = current_field (form);

    echo ();
    
    do
    {
        cgyshow ();             /* Display categories for this job  */
        CurXY (scrn, 0, 22);
        printw ("Enter Category # -0 to ADD new Category >> ");
#ifdef CURSES
        getnstr (tmpbuf, sizeof (tmpbuf));
        inpnum = atoi (tmpbuf);
#else
        inpnum = atoi (fgets (tmpbuf, sizeof (tmpbuf), stdin));
#endif
        if (!strcmp (tmpbuf, "0"))
        {
            addcgy ();
        }
    }
    while ((inpnum == 0) || (inpnum > (InCgyNum + ExCgyNum)));

    --inpnum;                   /* Reset to begin with category[0] */
  
    setbuf_cgy (form, inpnum);

    /* Assume we're in the edit form always.
     * May need to do something else later... */
    noecho ();
    
    if (form)
    {
        redrawwin ((WINDOW *)atoi (form_userptr(form)));
    }
    return (inpnum);
}
