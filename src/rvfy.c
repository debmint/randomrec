/* Recvfy: verify data and concatenate transactions file */

#define _GNU_SOURCE
#include "record.h"

#include <math.h>
#include <unistd.h>
#include <time.h>

#ifdef CURSES
#	define printf printw
#endif

#define MDIF .005

typedef struct mtrns
{
    struct mtrns *mup,
                 *older,
                 *newer;
    struct transact tdata;
}MTRANS;

extern struct transact TmpRec;

/* ******************************************************** *
 * DATcmp () - compares two DAT structures and returns the  *
 *   difference between the time-t's of the two             *
 * Passed : DAT t1, DAT t2                                  *
 * Returns: > 0 if t1 later than t2, < 0 if t1 before t2    *
 *          0 if equal                                      *
 * ******************************************************** */

time_t
DATcmp (DAT *t1, DAT *t2)
{
    struct tm tm1,
              tm2;

    memset (&tm1, 0, sizeof (struct tm));
    memset (&tm2, 0, sizeof (struct tm));

    tm1.tm_year = t1->Yr;    tm2.tm_year = t2->Yr;
    tm1.tm_mon  = t1->Mo;    tm2.tm_mon  = t2->Mo;
    tm1.tm_mday = t1->Da;    tm2.tm_mday = t2->Da;

    return difftime (mktime (&tm1), mktime (&tm2));
}

/* ******************************************************** *
 * db_export() - export flat-file structure to postgresql   *
 *            COPY - compatible text file                   *
 * Passed : nothing                                         *
 * Returns : nothing                                        *
 * ******************************************************** */

void db_export(void)
{
    FILE *incfile, *expfile, *ifile;
    char fname[200],
         *fnp;
    struct transact mytrns,
                    *trn;

    trn = &mytrns;
    
    clear();

    mvprintw (5,10,"Enter a name for the EXPORT FILE ");
    mvprintw (7,10,"Empty string to abort");
    mvprintw (8,10, "> ");
    wrefresh (mainwin);
    echo();

    do {
        getnstr(fname,sizeof(fname) - 1);
        
        if (!strlen(fname))  /* Null string represents abort */
        {
            return;
        }

        /* Open an incomes file */
        asprintf (&fnp, "%s_inc.txt", fname);
        incfile = fopen (fnp, "wb");
        free (fnp);
        
        /* ... and an expense file */
        asprintf (&fnp, "%s_exp.txt", fname);
        expfile = fopen (fnp, "wb");
        free (fnp);
        

    } while (!(incfile && expfile));
    
    if (!(ifile = fopen (datafil,"rb")))
    {
        fclose (incfile);
        fclose (expfile);
        return;
    }
    
    noecho();

    while (fread(trn, sizeof(struct transact), 1, ifile) == 1)
    {
        FILE *dst;
        struct cgystr *cg;

        if (trn->IsIncom)
        {
            dst = incfile;
            cg = &Income[trn->Cgy];
        }
        else
        {
            dst = expfile;
            cg = &Expense[trn->Cgy];
        }
        
        fprintf(dst, "%04d-%02d-%02d\t%s\t%s\t%s\t%s\t%s\t%s\t%f\n",
                         trn->Date.Yr+1900, trn->Date.Mo, trn->Date.Da,
                         trn->Descr,
                         strlen(trn->Quan) > 0 ? trn->Quan : "\\N",
                         strlen(trn->Invoic) > 0 ? trn->Invoic : "\\N",
                         strlen(trn->ChkNo) > 0 ? trn->ChkNo : "\\N",
                         Person[trn->ToFrm].Who,
                         cg->CgyNam,
                         trn->Cost);
    }

    fclose (ifile);
    fclose (expfile);
    fclose (incfile);
}

/* ******************************************************** *
 * rec_tsort (&mem_dat, &nilrec) - places this in sorted    *
 *    place in tree                                         *
 * Passed: base of tree, transact * to record to insert     *
 * Returns: address of new MTRANS                           *
 * ******************************************************** */

static MTRANS *
rec_tsort (MTRANS *tree, struct transact *me)
{
    MTRANS *newt;
    time_t tdiff;
    MTRANS *curpos = tree;

    while (1)
    {
        tdiff = DATcmp (&(me->Date), &((curpos->tdata).Date));
        
        if (tdiff < 0)
        {
            if (curpos->older)
            {
                curpos = curpos->older;
            }
            else
            {
                break;
            }
        }
        else
        {
            if (curpos->newer)
            {
                curpos = curpos->newer;
            }
            else
            {
                break;
            }
        }
    }

    if (!(newt = calloc (sizeof (MTRANS), 1)))
    {
        return NULL;
    }

    /* Now set up new entry */

    if (tdiff < 0)
    {
        curpos->older = newt;
    }
    else
    {
        curpos->newer = newt;
    }

    memcpy (&(newt->tdata), me, sizeof (struct transact));
    newt->mup = curpos;

    return newt;
}

/* ***************************************************** *
 * trans_write_node () - Parses the transactions tree    *
 *      and recursively calls itself to cover the entire *
 *      tree.                                            *
 * Passed : begin of node to trace back                  *
 *          FILE * to open file to write to (if writing) *
 *          NOTE: if the FILE * is NULL, then, of course *
 *          don't attempt to write.  This is useful for  *
 *          doing a cleanup after a failed session       *
 * ***************************************************** */

static void
trans_write_node (MTRANS *start, FILE *fp)
{
    MTRANS *ctrans;

    ctrans = start;

    if (ctrans->older)
    {
         trans_write_node (ctrans->older, fp);
    }

    ctrans->older = NULL;  /* Remove "older" fork from this node */
    
    if (fp)
    {
        fwrite (&(ctrans->tdata), sizeof (struct transact), 1, fp);
        auxupd (&(ctrans->tdata));
    }

    /* if a "newer" node, do it */

    if (ctrans->newer)
    {
        trans_write_node (ctrans->newer, fp);
    }

    ctrans->newer = NULL;   /* Remove "newer" fork from this node */
    free (ctrans);
}

/* ***************************************************** *
 * recvfy () - re-sort data and verify integrity of sums *
 * ***************************************************** */

void
recvfy (void)
{
    MTRANS *mem_data = (MTRANS *)NULL,
           *curtrns;
    
    struct transact nilrec;
    struct pay_p tPrs[MaxPers];
    struct _yrttl tYttl;
    struct cgystr tIn[MaxInc],
                  tEx[MaxExp];
    FILE *newpath;
    unsigned cnum;     /* Data file pointers for sort */
    long recsiz;

    if (!UpToDate)
    {
        updatfil ();
        UpToDate = TRUE;
    }

    clear ();
    printw ("Verify record... this may take some time....\n");
    standout();
    printw ("It is highly recommended you BACK UP your system first!\n");
    standend();
    printw ("\nARE YOU SURE ???  < Y > or < N >");

    if (yesno () != 'Y')
    {
        return;
    }

    /* Load all transactions into a memory tree */
    
    if (!(newpath = fopen (datafil,"rb")))
    {
        opn_err (datafil);
        return;
    }

    /* Initialize tree with first record */
    if ( fread (&nilrec, sizeof (struct transact), 1, newpath) != 1)
    {
        rd_err (datafil);
        return;
    }
    else
    {
        if (!(mem_data = calloc (sizeof (MTRANS), 1)))
        {
            rd_err ("malloc");
            return;
        }
        else {
            memcpy (&(mem_data->tdata), &nilrec, sizeof (struct transact));
            /* ->mup will be NULL here */ 
        }
    }

    /* Now read in all entries and sort in tree */


    while ( fread (&nilrec, sizeof (struct transact), 1, newpath) == 1)
    {
        if (nilrec.Cost) /* Weed out deleted entries here */
        {
            if (!(rec_tsort (mem_data, &nilrec)))
            {
                /* Try to free any memory that might have been allocated */
                trans_write_node(mem_data, NULL);
                rd_err ("malloc");
                return;
            }
        }
    }

    if (!feof (newpath))
    {
        rd_err (datafil);
        fclose (newpath);
        return;
    }

    fclose (newpath);
    
    zilchrec (&nilrec);

    /* Clear out structures */
    memcpy (tPrs, Person, sizeof (tPrs));

    for (cnum = 0; cnum < NumPayrs; cnum++)
    {
        Person[cnum].GotFrom = Person[cnum].PdTo = 0;
    }

    memcpy (tIn, Income, sizeof (Income));

    for (cnum = 0; cnum < InCgyNum; cnum++)
    {
        Income[cnum].CgyTot = 0;
    }

    memcpy (tEx, Expense, sizeof (Expense));

    for (cnum = 0; cnum < ExCgyNum; cnum++)
    {
        Expense[cnum].CgyTot = 0;
    }
    
    tYttl = YrTtl;
    YrTtl.TotXpns = YrTtl.TotInc = 0;

    if (rename (datafil, "datafil.old") == -1)
    {
        opn_err ("can't rename");
        return;
    }

    if (!(newpath = fopen (datafil, "w")))
    {
        printw ("Error # %d\n", errno);
        getch ();
        opn_err ("temporary file");
        return;
    }
    else
    {
        errno = 0;
    }

    /* Now read and sort through database */

    curtrns = mem_data;
    trans_write_node(mem_data, newpath);
    
    /* Now verify that all data is valid */

    UpToDate = TRUE;

    for (cnum = 0; cnum < MaxPers; cnum++)
    {
        if ((fabs (Person[cnum].GotFrom - tPrs[cnum].GotFrom) > MDIF) ||
            (fabs (Person[cnum].PdTo - tPrs[cnum].PdTo)) > MDIF)
        {
            printw ("Non-match in pyr#%d: %s Frm[was:%9.2f %9.2f]\n",
                    cnum, Person[cnum].Who,
                    tPrs[cnum].GotFrom, Person[cnum].GotFrom);
            printw ("   To[was:%9.2f %9.2f]\n",
                    tPrs[cnum].PdTo, Person[cnum].PdTo);
            UpToDate = FALSE;
        }
    }

    for (cnum = 0; cnum < MaxInc; cnum++)
    {

        if (fabs (Income[cnum].CgyTot - tIn[cnum].CgyTot) > MDIF)
        {
            printw ("Non-match in: %-20s was:%9.2f %9.2f\n",
                    Income[cnum].CgyNam,
                    tIn[cnum].CgyTot, Income[cnum].CgyTot);
            UpToDate = FALSE;
        }

        if (fabs (Expense[cnum].CgyTot - tEx[cnum].CgyTot) > MDIF)
        {
            printw ("Non-match in: %-20s was: %9.2f now: %9.2f\n",
                    Expense[cnum].CgyNam,
                    tEx[cnum].CgyTot, Expense[cnum].CgyTot);
            UpToDate = FALSE;
        }
    }

    if (fabs (tYttl.TotXpns - YrTtl.TotXpns) > MDIF)
    {
        printw ("Non-match in YrTtls: Income=was: %9.2f now: %9.2f \n",
                tYttl.TotInc, YrTtl.TotInc);
        UpToDate = FALSE;
    }

    if (fabs (tYttl.TotInc - YrTtl.TotInc) > MDIF)
    {
        printw ("Non-match in YrTtls: Expense=was: %9.2f now: %9.2f\n",
                tYttl.TotXpns, YrTtl.TotXpns);
        UpToDate = FALSE;
    }

    if (!UpToDate)
    {
        printw ("\nDo you wish to Update totals files < Y/N )?  ");
        if (yesno () == 'Y')
        {
            updatfil ();
        }
        else
        {
            /* Restore tallies to what they were before */

            memcpy (Person, tPrs, sizeof (tPrs));
            memcpy (Income, tIn, sizeof (Income));
            memcpy (Expense, tEx, sizeof (Expense));
            YrTtl = tYttl;
        }
    }
    
    if ((recsiz = _gs_size (newpath)) == -1)
    {

        printw ("Couldn't get filesize of new data file\n");
        printw ("Press a key...");
        refresh ();
        getch ();
    }
    else
    {
        LstRec = (int) (recsiz / (long) sizeof (struct transact));
    }
    
    fclose (newpath);

}
