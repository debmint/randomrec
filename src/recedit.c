/* ********************************************* *
 * Recedit :  edit functions for record file     *
 * ********************************************* */

/* $Id$ */

#include <form.h>
#include <curslib.h>

#include <ctype.h>
#include "record.h"
#include <strings.h>

/* make it easy to remember fields */
enum
{
    dscr_fld,
    qty_fld,
    invc_fld,
    ckno_fld,
    cost_fld,
    cgy_fld,
    tf_fld,
    date_fld,
    N_FIELDS
};

int dates[3];

#define ewin edit_win->top_win

char Title[36];
struct transact TmpRec;

static int
PickRec (char *fnct, char *pmpt, struct transact *trns)
{
    char inpbuf[6];
    register int recnum;

    inpbuf[0] = '\0';

    do
    {
        do
        {
            clear ();
            mvprintw (3, 5, "File to %s\n", fnct);
            mvprintw (5, 5, "Enter a number: ");
            attrset (COLOR_SET (COLOR_WHITE, COLOR_RED) | A_BOLD);
            printw ("[ 1 ]");
            attrset (A_NORMAL);
            printw ( " to ");
            attrset (COLOR_SET (COLOR_WHITE, COLOR_RED) | A_BOLD);
            printw ("[ %d ]", LstRec);
            attrset (A_BOLD);
            mvprintw (7, 18, "-OR-");
            attron (COLOR_SET (COLOR_WHITE, COLOR_RED));
            mvprintw (9, 5, "[ S ]");
            attrset (A_NORMAL);
            printw (" to search --      ");
            attrset (COLOR_SET (COLOR_WHITE, COLOR_RED) | A_BOLD);
            printw ("[ 0 ]");
            attrset (A_NORMAL);
            printw (" to abort.. ");
            getnstr (inpbuf, sizeof (inpbuf));
            
            if (index (inpbuf, 'S') || index (inpbuf, 's'))
            {
                if (((recnum = FilMatch (pmpt, trns)) >= 0) &&
                              (recnum <= LstRec))
                    return recnum;
            }
            else
            {
                recnum = atoi (inpbuf);

                /* 0 is reserved for "abort" */
                if (recnum == 0)
                {
                    return LstRec + 1;
                }
            }
        }
        while ((!recnum) || (recnum > LstRec));

        --recnum;  /* adjust recnum to 0-based offset */
        
        if (!(fpath = fopen (datafil, "r")))
        {
            return (-1);
        }

        if (fseek (fpath, (long) (sizeof (struct transact) * (recnum - 1)),
                          0) == -1)
        {
            fclose (fpath);
            return (-1);
        }

        if (fread (trns, sizeof (struct transact), 1, fpath) != 1)
        {
            fclose (fpath);
            return (-1);
        }

        fclose (fpath);
        recdspl (pmpt);
        printw ("\nIs this the correct one ? <Y/N> ");
    }
    while (yesno () == 'N');

    return (recnum);
}

void
recdspl (char *title_)
{
    register int t_loc = 25 - strlen (Title) / 2;

    /*  Print Title  */
    cls;
    CurXY (scrn, t_loc, 0);
    ReVOn (scrn);
    printw ("%s", title_);
    ReVOff (scrn);

    /* Print formatted data */
    wrtdate ();
    wrtdscr ();
    wrtcgy ();
    wrtquan ();
    wrtinvc ();
    wrtchk ();
    wrtwho ();
    wrtcst ();
    refresh ();
}

/* Verify that you really want to quit/save
* returns original keypress if yes, 0 if no
*/

static char
edquit (char opt, char *pmpt)
{
    char yn;

    CurXY (scrn, 0, 17);
    ErEOScrn (scrn);
    printw ("%s?  Are you sure??? <Y/N>", pmpt);
    switch (yn = yesno ())
    {
    case 'Y':
        return (opt);
    default:
        return (0);
    }
}

/* Save currently-worked-on record to file */

static int
recsav (struct transact *recptr, int edrec)
{
    char fnam[80];
    register int errflg = -1;

    if (!(fpath = fopen (datafil, "r+")))
    {
        opn_err (fnam);
    }
    else
    {
        if (fseek
            (fpath, (long) (edrec - 1) * (long) sizeof (struct transact), 0))
        {
            printw ("Seek error in data file\nAborting without saving...\n");
        }
        else
        {
            if (!fwrite (recptr, sizeof (struct transact), 1, fpath))
            {
                wrt_err ("data file");
            }
            else
            {
                if (recptr->Cost)       /* Only if Cost non-zero */
                    auxupd (recptr);
                errflg = 0;
            }
        }
        fclose (fpath);
    }

    return (errflg);
}

void
auxupd (struct transact *recptr)
{
    if (recptr->IsIncom)
    {
        Person[recptr->ToFrm].GotFrom += recptr->Cost;
        Income[recptr->Cgy].CgyTot += recptr->Cost;
        YrTtl.TotInc += recptr->Cost;
    }
    else
    {
        Person[recptr->ToFrm].PdTo += recptr->Cost;
        Expense[recptr->Cgy].CgyTot += recptr->Cost;
        YrTtl.TotXpns += recptr->Cost;
    }
}

void
zilchrec (struct transact *t_rec)
{
    memset (t_rec, 0, sizeof (t_rec));
    t_rec->Date = Today;
    t_rec->Date.Yr = RecYr;     /*  but make it for RecYr  */
}

void
wrtdate (void)
{
    register int yyr = Rec.Date.Yr;

    if (recinit) return;

    CurXY (scrn, 4, 2);
    ErEOLine (scrn);
    printw ("Traction Date :  ");
    printw ("%02d/%02d/%02d", Rec.Date.Mo, Rec.Date.Da, yyr % 100);
#ifdef CURSES
    refresh ();
#endif

}

void
wrtdscr (void)
{
    if (recinit) return;

    CurXY (scrn, 1, 3);
    ErEOLine (scrn);
    printw ("Item Description : ");
    printw ("%-s", Rec.Descr);
#ifdef CURSES
    refresh ();
#endif

}

void
wrtcgy (void)
{
    register struct cgystr *cgyptr;
    register int ofst;

    if (recinit) return;

    CurXY (scrn, 9, 4);
    ofst = (int) (Rec.Cgy);
    if (Rec.IsIncom)
    {
        cgyptr = &Income[ofst];
    }
    else
    {
        cgyptr = &Expense[ofst];
    }
    ErEOLine (scrn);
    printw ("Category : ");
    printw ("%-s", cgyptr->CgyNam);
#ifdef CURSES
    refresh ();
#endif
}

void
wrtquan (void)
{
    if (recinit) return;

    CurXY (scrn, 9, 5);
    ErEOLine (scrn);
    printw ("Quantity : ");
    printw ("%-s", Rec.Quan);
#ifdef CURSES
    refresh ();
#endif
}

void
wrtinvc (void)
{
    if (recinit) return;

    CurXY (scrn, 10, 6);
    ErEOLine (scrn);
    printw ("Invoice : ");
    printw ("%-s", Rec.Invoic);
#ifdef CURSES
    refresh ();
#endif
}

void
wrtchk (void)
{
    if (recinit) return;

    CurXY (scrn, 10, 7);
    ErEOLine (scrn);
    printw ("Check # : ");
    printw ("%-s", Rec.ChkNo);
#ifdef CURSES
    refresh ();
#endif
}

void
wrtwho (void)
{

    if (recinit) return;

    CurXY (scrn, 10, 8);
    ErEOLine (scrn);
    printw ("To/From : ");
    printw ("%-s", Person[(int) Rec.ToFrm].Who);
#ifdef CURSES
    refresh ();
#endif
}

void
wrtcst (void)
{
    if (recinit) return;

    CurXY (scrn, 5, 9);
    ErEOLine (scrn);
    printw ("Cost / Price : ");
    printw ("%9.2f", Rec.Cost);
#ifdef CURSES
    refresh ();
#endif
}

        /* FilMatch() : Searches through database for match of input */

int
FilMatch (char *pmpt, struct transact *trns)
{
    char b,
         *strptr,
         date_str[12];
    int tim,
        dsc,
        recno = 0;
    DAT mytime;

    tim = dsc = 0;
    date_str[0] = '\0';
    mytime.Mo = Today.Mo;
    mytime.Da = Today.Da;
    mytime.Yr = Today.Yr;

    do
    {
        clear ();
        
        if (tim)
        {
            printw ("\nCurrent Search Date: %02d/%02d/%d\n",
                    mytime.Mo, mytime.Da, mytime.Yr);
        }

        printw ("\n 'D' - Match Date\n");

        if (dsc)
        {
            printw ("\nCurrent Search Pattern: %s\n", tmpstr);
        }

        printw (" 'E' - Match Description\n\n");

        if (tim | dsc)
        {
            printw (" 'S' - Begin Search\n\n");
        }

        printw (" 'Q' - Abort Search\n\n");

        b = getch ();

        if ((b = toupper (b)) == 'Q')
        {
            return (LstRec + 1);
        }

        switch (b)
        {
        case 'D':
            /*ed_dat (&mytime);*/

            noecho ();
            wdg_get_date ((WINDOW *)NULL, mytime.Yr % 100,
                                          mytime.Mo,
                                          mytime.Da,
                                          date_str);
            echo ();
            
            if (strlen (date_str))
            {
                ++tim;
                sscanf (date_str, "%d/%d/%d", &mytime.Yr,
                                              &mytime.Mo,
                                              &mytime.Da);

                if (mytime.Yr >= 1900)
                {
                    mytime.Yr -= 1900;
                }
                else {
                    if (mytime.Yr < 70)
                    {
                        mytime.Yr += 100;
                    }
                }
            }

            break;
        case 'E':
            noecho ();
            strptr = wdg_input_box ("Enter a Description",
                                    sizeof (Rec.Descr));
            echo ();
            strcpy (tmpstr, strptr);
            free (strptr);
            ++dsc;
            
            /*ed_dscr ((struct transact *)NULL, tmpstr);*/    /* in data (defined in rec.h) */
            break;
        }
    }
    while (b != 'S');

    if (dsc)
    {
        register char *pp = tmpstr;

        while (*pp)
        {
            if (isalpha (*pp))
            {
                *pp = toupper (*pp);
            }

            ++pp;
        }
    }

    if (!(fpath = fopen (datafil, "r")))
        return (-1);

    for (;;)
    {
        ++recno;
        if (fread (trns, sizeof (struct transact), 1, fpath) != 1)
        {
            if (feof (fpath))
            {
                printw ("End of record reached with no match\n");
                if (tim)
                {
                    printw ("No match for %d %d %d\n", mytime.Yr, mytime.Mo, mytime.Da);
                }
                if (dsc)
                {
                    printw ("No match for %s\n", tmpstr);
                }
                printw ("Press a key\n");
                getch ();
            }
            fclose (fpath);
            return (LstRec + 1);  /* Flag illegal record # to continue */
        }

        if (tim && ((mytime.Yr != (trns->Date).Yr) ||
                    (mytime.Da != (trns->Date).Da) ||
                    (mytime.Mo != (trns->Date).Mo)))
        {
            continue;
        }

        if (dsc)
        {
            register char *pp = trns->Descr;
            register char *dsp = tmps2;

            while ((*dsp++ = toupper (*pp++)))
            {
            }

            if (!strstr (tmps2, tmpstr))
            {
                continue;
            }
        }

        Rec = *trns;  /* temporary fix for old recdslp */
        recdspl (pmpt);
        printw ("\nIs this the correct one ? <Y/N> ");

        if (yesno () == 'Y')
        {
            fclose (fpath);
            return recno;
        }
    }
}

/* ************************************************************* *
 * rec_create_menu_tofrom() - creates, fills a menu              *
 *     of the payor/payee list, with a "ADD Person" as the first *
 *     element, and "Cancel" as the second.  The window is       *
 *     automatically sized for the screen.  Thus a return index  *
 *     of 0 will mean "Add a user", 1 for "Cancel", or the index *
 *     of the user list +2                                       *
 * Returns: with the menu ready to post (allowing any other      *
 *           preparations to be made before posting              *
 * ************************************************************* */

void
rec_create_menu_tofrom (WINDOW *prevwin, WINDOW **MnuWin, ITEM ***py_item,
                        MENU **py_menu, int *mCols, WDGT_RING **ring)
{
    int mwinH, mwinW,   /* main window Height, Width */
        mnuH,  mnuW;    /* menu window Height, Width */

    int i;

    (*py_item) = calloc ((NumPayrs + 3), sizeof (ITEM *));
   
    (*py_item)[0] = new_item ("Add Payor/Payee", " ");
    (*py_item)[1] = new_item ("Cancel", " ");
    
    for (i = 0; i < NumPayrs; i++)
    {
        (*py_item)[i + 2] = new_item (Person[i].Who, " ");
    }
    (*py_item)[NumPayrs + 2] = (ITEM *)NULL;

    *py_menu = new_menu((ITEM **)(*py_item));

    /* Now calculate some window heights */
    /* Try to make "neat-looking" window */
    if (NumPayrs <= LINES - 4)
    {
        *mCols = 1;
    }
    else
    {
        if (NumPayrs < (LINES - 4) * 2)
        {
            *mCols = 2;
        }
        else {
            *mCols = 3;
        }
    }

    scale_menu (*py_menu, &mnuH, &mnuW);
    
    mnuH += 2; mnuW += 2;
    
    if (mnuH > (LINES - 6))
    {
        mnuH = LINES -6;
    }
    
    mwinH = mnuH + 4;
    mwinW = mnuW + 4;

    *MnuWin = newwin (mwinH, mwinW, (LINES - mwinH)/2, (COLS - mwinW)/2);
    keypad (*MnuWin, TRUE);
    wbkgdset (*MnuWin, COLOR_SET (COLOR_BLACK, COLOR_YELLOW));
    wclear (*MnuWin);

    set_menu_win(*py_menu, *MnuWin);
    set_menu_sub (*py_menu, derwin (*MnuWin, mnuH, mnuW, 2, 2));

    set_menu_format (*py_menu, mnuH, *mCols);
    /*menu_opts_off (*py_menu, O_SHOWDESC | O_NONCYCLIC);*/
    menu_opts_off (*py_menu, O_NONCYCLIC);
    
    box (*MnuWin, 0, 0);
    wattron (*MnuWin, COLOR_SET (COLOR_WHITE, COLOR_BLUE));
    mvwprintw (*MnuWin, 1, (mnuW - 20)/2, "Choose a Payor/Payee");
    wattrset (*MnuWin, A_NORMAL);

    /* Now create the ring */
    *ring = widget_ring_new (*MnuWin, 2, 2, 0, 0);
    
    widget_ring_append (*ring, mwinH - 2, mwinW/4, "Accept", 1);
    widget_ring_append (*ring, mwinH - 2, mwinW/2, "Cancel", -1);

}

/* ************************************************************* *
 * rec_clear_tofrom_menu () - unpost a to_from menu and free all *
 *        the items allocated to it                              *
 * ************************************************************* */

void
rec_clear_menu_tofrom (WINDOW **MnuWin, MENU **py_menu, ITEM ***py_item)
{
    int i;
    
    if (!(MnuWin && *MnuWin && py_menu && *py_menu && py_item && *py_item))
    {
        return;
    }
    /* Close out the menu */
    
    unpost_menu(*py_menu);
    free_menu (*py_menu);

    for (i = 0; i < NumPayrs; i++)
    {
        free_item ((*py_item)[i]);
    }

    delwin (*MnuWin);
}

/* ************************************************************* *
 * rec_add_tofrom () - add an entry to the Payor/Payee list      *
 * Passed :  WINDOW * - the window over which the list is drawn  *
 *           WINDOW ** MnuWin - the Menu "container" window      *
 *           (ITEM **)* - pointer to the item **                 *
 *           (MENU *)*  - pointer to the MENU *                  *
 *           *mCOLS - the address of the columns # (in caller)   *
 *           (WDGT_RING *) - ptr to the WDGT_RING * (int caller) *
 * NOTE   :  If the MnuWin ptr is NULL, it's asssumed that a     *
 *           menu has not been called and a p/p menu is created  *
 *           and posted, and destroyed on exit                   *
 * Return Condition :  "Cancel" pressed Nothing changed          *
 *           "Add" pressed Payor/Payee list is updated.          *
 *           If a MnuWin was passed, this one is destroyed and   *
 *             a new, updated MnuWin is created for continuation *
 * NOTE   :  If a new entry is added, don't forget to free()     *
 *           the returned string on return from function.
 * ************************************************************* */

void
rec_add_tofrom (WINDOW *prevwin, WINDOW **MnuWin, ITEM ***py_item,
                        MENU **py_menu, int *mCols, WDGT_RING **ring)
{
    char *NewPers;
    
    /* Avoid segfault */

    if (MnuWin)
    {
        if ((!py_item) || (!py_menu) || (!mCols) || (!ring))
        {
            /* Add error reporting */
            return;
        }
    }

    NewPers = wdg_input_box ("Enter new Payor/Payee Name",
                             sizeof (Person[0].Who));
    if (NewPers)
    {
        strcpy (Person[NumPayrs].Who, NewPers);
        
        free (NewPers);

        /* If a menu was displayed, we now need to recreate it
         * with the new entry added */
        /* NOTE: this needs to be done before NumPayrs is updated */

        if (MnuWin)
        {
            rec_clear_menu_tofrom (MnuWin, py_menu, py_item);
        }
        
        ++NumPayrs;

        /* Now recreate the new menu with the updated NumPayrs */

        if (MnuWin)
        {
            rec_create_menu_tofrom (prevwin, MnuWin, py_item,
                                    py_menu, mCols, ring);
        }
    }
}

/* ************************************************** *
 * rec_get_to_from() -                                *
 * Returns:  see rec_get_cgy() for return conditions  *
 * ************************************************** */

void
rec_get_to_from(FORM *form)
{
    ITEM **py_item;
    MENU *py_menu;
    FIELD *py_field;
    WINDOW *prevwin,
           *MnuWin;
    WDGT_RING *ring,
              *cur_ring;

    int mCols;
    int mIdx;           /* The index for the menu item */
    
    py_field = current_field (form);
    prevwin = (WINDOW *)atoi (form_userptr(form));

    rec_create_menu_tofrom (prevwin, &MnuWin,
                            &py_item, &py_menu, &mCols, &ring);
    cur_ring = ring;

    wrefresh (MnuWin);
    post_menu (py_menu);
    
    mIdx = wdg_menu_run(py_menu, &cur_ring, mCols);

    mIdx -= 2;   /* Remove the offset created by first 2 elements */
    
    if ((mIdx != -1) && (mIdx != -3))  /* Either Cancel pressed) */
    {
        if (mIdx == -2)
        {
            /* Add a payor/payee */
            rec_add_tofrom (prevwin, &MnuWin, &py_item,
                            &py_menu, &mCols, &ring);
        }
            /* Handle the returned index # either from main p/p menu
             * or from the add payor/payee menu */
        if (mIdx)
        {
            set_field_buffer (py_field, 0, Person[mIdx].Who);
            
            sprintf (field_userptr (py_field), "%d", mIdx);
        }
    }

    /* Close out menu */
    rec_clear_menu_tofrom (&MnuWin, &py_menu, &py_item);

    if (prevwin)
    {
        redrawwin (prevwin);
    }
}

/* ************************************************** *
 * rec_get_cgy() -                                    *
 * Returns: with category STRING stored in the cgy    *
 *    field's buffer on "ACCEPT".  The index will be  *
 *    found by a strcmp() to be saved in the file     *
 * ************************************************** */

void
rec_get_cgy (FORM *form)
{
}

/* ************************************************** *
 * rec_get_date() - call wdg_get_date() to input date *
 * Returns: with date saved in the date's field       *
 *     buffer if "ACCEPT", else no change if "CANCEL" *
 * ************************************************** */

void
rec_get_date (FORM *form)
{
    char date_got[12];
    FIELD *dfld = current_field (form);

    int yr = 0,
        mo = 0,
        da = 0;
   
    /* Ignore if not in date entry field */
    if (field_index (dfld) != date_fld)
    {
        return;
    }

    date_got[0] = '\0';
    
    sscanf (field_buffer (dfld, 0), "%d/%d/%d", &yr, &mo, &da);
    wdg_get_date (NULL, yr, mo, da, date_got);
    
    if (strlen (date_got))
    {
        set_field_buffer (dfld, 0, date_got);
    }
    
    redrawwin ((WINDOW *)atoi(form_userptr (form)));
}

/* **************************************************** *
 * setbuf_tf() - Sets the tf_fld in the edit buffer     *
 * Passed:  The FIELD * for the field                   *
 *          The index of the To/From list               *
 *          It will set the Who string in buffer 0      *
 *          and set the userptr to the correct index    *
 * **************************************************** */

void
setbuf_tf (FIELD *field, int idx)
{
    char tmp[5];
    
    set_field_buffer (field, 0,
        Person[idx].Who);
    
    sprintf (tmp, "%d", idx);
    sprintf (field_userptr (field), "%d", idx);
}

/* ***************************************************** *
 * setbuf_cost() - Sets the field buffer for the cost    *
 * Passed:   The FIELD * to for the cost field           *
 *          the double value for the field               *
 * ***************************************************** */

void
setbuf_cost (FIELD *field, double cost)
{
    char tmp[15];

    sprintf (tmp, "%f", cost);
    set_field_buffer (field, 0, tmp);
}

/* ******************************************************** *
 * setbuf_cgy() - Sets the field buffer for the category    *
 * Passed :  The FIELD * to the categor field               *
 *               if NULL, then provide it                   *
 *           The index to the Incom/Expense list in         *
 *           "stacked" format - i.e. Expense on top of      *
 *           Income                                         *
 * ******************************************************** */

void
setbuf_cgy (FORM *form, int idx)
{
    FIELD **field_list;
    char tmp[5];
    int realidx = idx;
    struct cgystr *my_cgy = Income;

    field_list = form_fields (form);
    
    sprintf (tmp, "%d", idx);
    
    if (idx >= InCgyNum)
    {
        my_cgy = Expense;
        realidx -= InCgyNum;
    }

    my_cgy += realidx;

    set_field_buffer (field_list[cgy_fld], 0, my_cgy->CgyNam);
    sprintf (field_userptr (field_list[cgy_fld]), "%d", idx);;

    if (strlen (my_cgy->DscDf))
    {
        set_field_buffer (field_list[dscr_fld], 0, my_cgy->DscDf);
    }

    if (my_cgy->TfDf > 0)
    {
        setbuf_tf (field_list[tf_fld], (my_cgy->TfDf) - 1);
    }
}

/* **************************************************** *
 * rec_ed_hooks()  - the generic hook to which the form *
 *     jumps on entry into any field.  The fields are   *
 *     sorted here. a simple return from the oens that  *
 *     are not appplicable.                             *
 * Passed: Pointer to the form list                     *
 * **************************************************** */

void
rec_ed_hooks (FORM *form)
{
    int idx = field_index (current_field (form));

    if ((idx != date_fld) && (idx != cgy_fld) && (idx != tf_fld))
    {
        return;   /* Other fields don't need this */
    }

    if (idx == date_fld)
    {
        rec_get_date (form);
    }
    else
    {
        if (idx == cgy_fld)
        {
            /*setbuf_cgy (form, ed_cgy(form));*/
            ed_cgy(form);

        }
        else
        {
            rec_get_to_from (form);
        }
    }
}

#define FMTOP 3

/* ************************************************** *
 * edit_record() - edit a record using a curses form  *
 * ************************************************** */

void
edit_record (struct transact *recitem, int recnum)
{
    SHADOW_WINDOW *edit_win;
    FIELD *field[N_FIELDS + 1];
    FORM *rec_form;

    char win_str[20];       /* a place to hold the string value for widow */
    char date_str[12];
    DAT *mydate;
    struct transact olditem;
    
    int i, ch,
        errflg;

    int edwinH = 22,
        edwinW = 70;
    
    int post_err;
    int done = 0;   /* Flag that the input is done */
    
    noecho ();

    if (LINES < edwinH)
    {
        edwinH = LINES;
    }

    if (COLS < edwinW)
    {
        edwinW = COLS;
    }
    
    /* Create the fields */

    field[dscr_fld] = new_field (1, sizeof (Rec.Descr),        0,  0, 0, 0);
    field[qty_fld]  = new_field (1, sizeof (Rec.Quan),         2,  0, 0, 0);
    field[invc_fld] = new_field (1, sizeof (Rec.Invoic),       4,  0, 0, 0);
    field[ckno_fld] = new_field (1, sizeof (Rec.ChkNo),        6,  0, 0, 0);
    field[cost_fld] = new_field (1, 10,                        8,  0, 0, 0);
    field[cgy_fld]  = new_field (1, sizeof (Income[0].CgyNam), 4, 10, 0, 0);
    field[tf_fld]  =  new_field (1, sizeof (Person[0].Who),    6, 10, 0, 0);
    field[date_fld] = new_field (1, 8,                         8, 25, 0, 0);
    field[N_FIELDS] = NULL;

    for (i = 0; i < N_FIELDS; i++)
    {
        set_field_back (field[i],
                        A_UNDERLINE | COLOR_SET (COLOR_BLACK, COLOR_WHITE));
        /* Allocate a 15-byte field_userptr buffer */
        set_field_userptr (field[i], calloc (1, 15));
    }

    rec_form = new_form (field);

    set_field_type (field[cost_fld], TYPE_NUMERIC, 2, -100000.00, 100000.00);

    set_field_buffer (field[dscr_fld], 0, "");
    /* Create edit window */

    edit_win = create_window_cleared (edwinH, edwinW, 1, (COLS - edwinW)/2,
                                      COLOR_BLACK, COLOR_WHITE, TRUE);
    wbkgd (ewin, COLOR_SET (COLOR_BLACK, COLOR_WHITE));
    shadow_box (edit_win, 0, 0, edwinH, edwinW);
    keypad (ewin, TRUE);
    sprintf (win_str, "%d", (int)ewin);

    /* Save the Window pointer into the form(to keep the WINDOW *
     * in hooks
     */
    
    set_form_userptr ( rec_form, win_str);
    
    set_form_win (rec_form, ewin);
    set_form_sub (rec_form, derwin (ewin, 15, edwinW - 32, FMTOP, 15));

    wattrset (ewin, COLOR_SET (COLOR_WHITE, COLOR_BLUE));
    mvwprintw (ewin, 1, 16, "Enter/Edit a Record");
    wattrset (ewin, COLOR_SET (COLOR_BLACK, COLOR_WHITE));
    
    mvwprintw (ewin, FMTOP,   2, "Description:");
    mvwprintw (ewin, FMTOP+2, 2, "Quantity:");
    mvwprintw (ewin, FMTOP+4, 2, "Invoice #:");
    mvwprintw (ewin, FMTOP+6, 2, "Check #:");
    mvwprintw (ewin, FMTOP+8, 2, "Cost/Price:");
    mvwprintw (ewin, FMTOP+4, edwinW - 16, ": Category");
    mvwprintw (ewin, FMTOP+6, edwinW - 16 , ": To/From Whom");
    mvwprintw (ewin, FMTOP+8, edwinW - 16, ": Date");

    mvwprintw (ewin, edwinH - 2, (edwinW - 12)/3, "<F1> to Save");
    mvwprintw (ewin, edwinH - 2, ((2 * edwinW)/3) - 6, "<F5> to Cancel");

    /* Preload date */
    if (recitem)
    {
        mydate = &(recitem->Date);
    }
    else
    {
        if (Rec.Date.Yr)
        {
            mydate = &(Rec.Date);
        }
        else
        {
            mydate = &Today;
        }
    }

    sprintf (date_str, "%02d/%02d/%02d", (mydate->Yr) % 100,
                                         mydate->Mo,
                                         mydate->Da);
    set_field_buffer (field[date_fld], 0, date_str);
    wrefresh (ewin);

    if (recitem)
    {
        setbuf_tf (field[tf_fld], recitem->ToFrm);
        set_field_buffer (field[dscr_fld], 0, recitem->Descr);
        set_field_buffer (field[qty_fld], 0, recitem->Quan);
        set_field_buffer (field[invc_fld], 0, recitem->Invoic);
        set_field_buffer (field[ckno_fld], 0, recitem->ChkNo);
        setbuf_cost (field[cost_fld], recitem->Cost);
        setbuf_cgy (rec_form, recitem->Cgy);
    }
    else
    {
        /* Start with something for Payor/Payee to avoid problem */
        setbuf_tf (field[tf_fld], 0);
        set_current_field (rec_form, field[cgy_fld]);
        rec_ed_hooks (rec_form);
        set_current_field (rec_form, field[dscr_fld]);
    }
    
    /* Set some hooks */
    set_field_init (rec_form, rec_ed_hooks);
    
    post_err = post_form (rec_form);
    
    do
    {
        done = 0;
        
        switch (ch = wgetch (ewin))
        {
            case '\x0a':
            case '\x0d':
                form_driver (rec_form, REQ_NEXT_FIELD);
                form_driver (rec_form, REQ_END_LINE);
                break;
            case KEY_DOWN:
                form_driver (rec_form, REQ_END_LINE);
                form_driver (rec_form, REQ_NEXT_FIELD);
                break;
            case KEY_UP:
                form_driver (rec_form, REQ_PREV_FIELD);
                form_driver (rec_form, REQ_END_LINE);
                break;
            case KEY_RIGHT:
                form_driver (rec_form, REQ_RIGHT_FIELD);
                form_driver (rec_form, REQ_END_LINE);
                break;
            case KEY_LEFT:
                form_driver (rec_form, REQ_LEFT_FIELD);
                form_driver (rec_form, REQ_END_LINE);
                break;
            case KEY_BACKSPACE:
                form_driver (rec_form, REQ_DEL_PREV);
                form_driver (rec_form, REQ_END_LINE);
                break;
            case KEY_DC:
                form_driver (rec_form, REQ_DEL_CHAR);
                form_driver (rec_form, REQ_END_LINE);
                break;
            case 9:       /* TAB */
                break;
            case KEY_F(1):
                form_driver (rec_form, REQ_END_LINE);
                if (atof (field_buffer(field[cost_fld], 0)))
                {
                    done = WDG_RET_OK;
                }
                else
                {
                    /* warn that cost needs to be input */
                }
                break;
            case KEY_F(5):
                done = WDG_RET_CANCEL;
            default:
                form_driver(rec_form, ch);
                break;
        }
    } while (!done);

    if (done == WDG_RET_OK)
    {
        sscanf (field_buffer (field[date_fld], 0), "%02d/%02d/%02d",
                                                      &Rec.Date.Yr,
                                                      &Rec.Date.Mo,
                                                      &Rec.Date.Da);

        if (Rec.Date.Yr < 70)
        {
            Rec.Date.Yr += 100;
        }

        strncpyfld (Rec.Descr, field[dscr_fld], sizeof (Rec.Descr));
        strncpyfld (Rec.Quan, field[qty_fld], sizeof (Rec.Quan));
        strncpyfld (Rec.Invoic, field[invc_fld], sizeof (Rec.Invoic));
        strncpyfld (Rec.ChkNo, field[ckno_fld], sizeof (Rec.ChkNo));

        Rec.Cost = atof (field_buffer (field[cost_fld], 0));

        Rec.ToFrm = atoi ((char *)(field_userptr (field[tf_fld])));
        i = atoi ((char *)field_userptr (field[cgy_fld]));
        
        if (i >= InCgyNum)
        {
            Rec.IsIncom = 0;
            i -= InCgyNum;
        }
        else
        {
            Rec.IsIncom = 1;
        }

        Rec.Cgy = i;

        /* If we're editing a record, subtract the old values from
         * tallies */
        
        if (recitem)
        {
            memcpy( &olditem, recitem, sizeof (struct transact));
            olditem.Cost = -olditem.Cost;
            auxupd (&olditem);
        }
        else
        {
            recnum = LstRec + 1;
        }

        errflg = recsav (&Rec, recnum);

        if ((errflg == E_OK) && (recnum > LstRec))
        {
            ++LstRec;
        }

        UpToDate = FALSE;
    }

    unpost_form (rec_form);
    free_form (rec_form);

    for ( i = 0; i < N_FIELDS; i++)
    {
        free (field_userptr(field[i]));

        free_field (field[i]);
    }

    end_shadow_win (edit_win);
    clear ();
    refresh ();
    
    echo ();

}

/* **************************************************** *
 * rec_alter_record () - setup to call edit_record()    *
 *     gets a record from PickRec() and passed the      *
 *     ptr to the struct transact on to edit_record()   *
 * **************************************************** */

void
rec_alter_record (void)
{
    struct transact *trns;
    int recnm;  /* Record # (beginning with 1) */
    
    trns = (struct transact *)calloc (1, sizeof (struct transact));
    
    recnm = PickRec ("Edit", "File to Edit: ", trns);

    if ((recnm <= LstRec) && (recnm != -1))
    {
        edit_record (trns, recnm);
    }

    free (trns);
}

/* **************************************************** *
 * rec_delete_record () - get a recpord number from     *
 *     PickRec() and if not flagged as an abort, remove *
 *     tallies for it and insert a null-filled field    *
 *     in the old position in the data file             *
 * **************************************************** */

void
rec_delete_record (void)
{
    struct transact *trns;
    int recnm;

    trns = (struct transact *)calloc (1, sizeof (struct transact));

    recnm = PickRec ("Delete", "File to Delete: ", trns);

    if ((recnm <= LstRec) && (recnm != -1))
    {
        trns->Cost = -(trns->Cost);
        auxupd (trns);
        memset (trns, '\x0', sizeof (struct transact)); /* Null out field */
        recsav (trns, recnm);  /* Save a null field */
        UpToDate = FALSE;
    }

    free (trns);
}
