/* ******************************************* *
 *                                             *
 * Record.c :  Main routine for "record",      *
 * a record-keeping application program        *
 * ******************************************* */

/* $Id$ */

/* ************************************************************* *
 * History :  02/28/2001 Changed format for "recstuff.mn" file   *
 *			01/23/2001	Version 2                                *
 *						Added default fields to income & expense *
 *							data to allow default inputs         *
 *			02/01/99	Fixed additional Yr displays, other bugs *
 *			04/10/99	Redid Date to take care of year 2000     *
 *						Now, year is x + 1900  (2001 is 101)     *
 *			10/02/97	Began First Version for G-Windows        *
 *          06/05/94	Began porting to OSK                     *
 *          03/22/92	First working "C" version                *
 *          ??/??/??    first version was written in Basic-09    *
 *                                                               *
 * ************************************************************* */

#define MAIN

char *MenFmt = "%s %s2%s40\n";

#include <ctype.h>
#include <unistd.h>
#include "record.h"

static void
menlin (char num, char pmpt[])
{
    printw ("       ");

    ReVOn (scrn);
    printw ("%c", num);
    ReVOff (scrn);
    printw ("%c%s\n", ' ', pmpt);
}

static void
err_mon (char *sbr)
{
    printw ("Error #%d in %s() routine\nPress [ENTER]...", errno, sbr);
    getch ();
}

static void
editpay (void)
{
    printw ("Payor/payee edit has not been implemented yet.\n");
    printw ("Please press <ENTER> to proceed ...\n");
    getch ();
}

/* Pick job and load its data
 *  returns ptr to chosen Job struct on success, null on error
*/

static struct job_index *
chgacct (void)
{

    /* First, if totals files not up-to-date, update them */
    if (!UpToDate)
    {
        updatfil ();
    }
    if (!(jobptr = pikacct (Job)))
    {
        quitrec ();
    }

    if (InSubDir)
    {
        if (chdir (".."))
        {
            fprintf (stderr,"Error # %d: \nCannot \"chdir\" to \"..\"\n",
                            errno);
            exit (errno);
        }
        InSubDir = FALSE;
    }

    if (chdir (jobptr->JobDir))
    {
        fprintf (stderr,"Error # %d: \nCannot \"chdir\" to: %s for %s\n",
                        errno, jobptr->JobDir, jobptr->JobNam);
        quitrec ();
    }
    else
    {
        InSubDir = TRUE;
    }
    
    fildir[0] = '\x0';          /* So fildir will be null */
    
    if (loaddat () == ERR)
    {
        fprintf (stderr, "Error loading data files...\nQuitting...\n");
        quitrec ();
    }

    return (jobptr);
}

/* ************************** *
 *   program entry point      *
 * ************************** */

int
main (int argc, char **argv)
{
    int ty;
    register int ylin;          /* OSK Y cursor positioning */

    recsetup ();                /* Go set terminal opts for program */

    /* Read in "recstuff" file   */

    UpToDate = TRUE;

    /* Test if recstuff already exists */
    if (!(fpath = fopen (stuff, "r")))
    {
        Bell (scrn);
        fprintf (stderr,
              "\nThe file \"recstuff\" cannot be found\n");
        /*quitrec ();
        exit (errno);*/
    }
    else
    {
        if ((fscanf (fpath, "Yr=%d\n", &ty)) == -1)
        {

            /* If can't read one byte (RecYr) */
            fprintf (stderr, "%s\n%s\n", "Can't read RecYr from recstuff\n",
                     "You are either not in a working directory");
        }
        else
        {
            RecYr = ty - 1900;
            printw ("\nRecord is for year: %d\n", ty);

            if ((NumJobs = loadjobs (Job)) == ERR)
            {
                rd_err (stuff);
                fclose (fpath);
                exit (errno);
            }

            if (fclose (fpath) == EOF)
            {
                clos_err (stuff);
                exit (errno);
            }
        }
    }

    if (!(jobptr = chgacct ()))
    {
        quitrec ();
    }

/* *************************************************** *
 *               Main Menu loop                        *
 * *************************************************** */

    for (;;)
    {
        mvwprintw (mainwin, 1, 40 - strlen (jobptr->JobNam) / 2,
                   jobptr->JobNam);
        wmove (mainwin, 18,46);
        wrefresh (mainwin);
        ans = getch ();

        /* Do choices */
        if (isalpha (ans))
            ans = toupper (ans);
        switch (ans)
        {
        case '1':
            /*recedit (ADD);*/      /* Do edit with NO EDIT flag */
            edit_record ((struct transact *)NULL, 0);
            break;
        case '2':
            /*recedit (EDIT);*/     /* Do edit with EDIT flag "on" */
            rec_alter_record ();
            break;
        case '3':
            /*recedit (DELETE);*/   /* Do edit with "DELETE" flag */
            rec_delete_record ();
            break;
        case '4':
            editpay ();
            break;
        case '5':
            ChDflts ();
            break;
        case '6':
            recvfy ();
            break;
        case '7':
            isprntr = FALSE;
            if (recprint ())
                err_mon ("recprint");
            break;
        case '8':
            isprntr = TRUE;
            if (recprint ())
                err_mon ("recprint");
            break;
        case 'C':
            if (!(jobptr = chgacct ()))
            {
                exit (errno);
            }
            break;
        case 'X':
            db_export();
            break;
        case 'Q':

            /* First, if totals files not up-to-date, update them */

            if (!UpToDate)
            {
                updatfil ();
            }
            quitrec ();
            break;
        default:
            break;
        }

        redrawwin (mainwin);
    }                           /*  End of menu loop   */
}

/* ************************** *
 *     End of main() loop     *
 * ************************** */
