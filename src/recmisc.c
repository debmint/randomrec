
/* Miscellaneous routines used in record program */

#include "record.h"

#include <time.h>
#include <ctype.h>
#include <unistd.h>

/* ************************************** *
 * curses_print_optlist() print a set     *
 *    of menu options                     *
 * Passed : initial xpos, ypos            *
 *          ptr to array of strings       *
 *          initial option number         *
 * Returns: nothing, parameters destroyed *
 * ************************************** */

static void
curses_print_optlist(int ypos, int xpos, char **list, int optno)
{
    while (**list )
    {
        attrset (COLOR_PAIR (COLOR_GREEN) | A_BOLD);
        mvprintw (ypos, xpos, " %d ", optno++);
        attrset (A_NORMAL);
        printw (" %s", *(list++));
        ++ypos;
    }
}

/* ***************************************** *
 * curses_create_print_window() - build the  *
 *    print window - we'll build it on call  *
 *    for now to see how fast it builds...   *
 * ***************************************** */

void
curses_create_print_window (void)
{
    char *printopts[] = { "Entire File",
                          "Single Category",
                          "General Summary",
                          "Payor Payee Summary",
                          "Monthly Summary",
                          "Return to Main Menu",
                          ""};
   
    if (!printwin)
    {
        curses_print_optlist (3, 30, printopts, 1);
        mvprintw (10, 30, "Choose an option:");
    }
}

/* ****************************************** *
 * curses_create_main_window() build the main *
 *       window.  It is a static window that  *
 *       is present at all times              *
 * ****************************************** */

static void
curses_create_main_window(void)
{
    char *dataopts[] = {"Add / Enter Data",
                        "Alter Data Entry",
                        "Delete Data Entry",
                        "Alter PAYOR/PAYEE List",
                        "Change DEFAULT inputs",
                        "VERIFY DATA",
                        ""};
    char *viewopts[] = {"Print to Screen", "Print Hardcopy", ""};
    
    if (mainwin)
    {
        return;
    }

    clear();
    
    attrset (A_STANDOUT | COLOR_PAIR (COLOR_WHITE));
    mvprintw (0, 20, " R E C O R D   E N T R Y  S Y S T E M ");
    attrset (A_NORMAL);
    
    attron (COLOR_PAIR (COLOR_CYAN) | A_UNDERLINE);
    mvprintw (4, 5, "Data Operations");
    attrset (A_NORMAL);
    curses_print_optlist (6, 5, dataopts, 1);

    attron (COLOR_PAIR (COLOR_CYAN) | A_UNDERLINE);
    mvprintw (4, 44, "View Options"); 
    attrset (A_NORMAL);
    curses_print_optlist (6, 45, viewopts, 7);

    /* Now Quit/Change opts */
    attrset (COLOR_PAIR (COLOR_GREEN) | A_BOLD);
    mvprintw (14, 28, " C ");
    attrset (A_NORMAL);
    printw (" hange Accounts");

    /* Database Export */
    mvprintw (15, 26, " Database E");
    attron (COLOR_PAIR (COLOR_GREEN) | A_BOLD);
    printw(" X ");
    standend();
    printw ("port");

    attron (COLOR_PAIR (COLOR_GREEN) | A_BOLD);
    mvprintw (16, 28, " Q ");
    standend();
    attrset (A_NORMAL);
    printw (" UIT");
    mvprintw (18, 28, "MAKE SELECTION  ->");
   
    /* Now do borders */
    attron (A_BOLD);
    mvaddch (3, 2, ACS_ULCORNER);
    hline (ACS_HLINE, 75);
    mvaddch (3, 39, ACS_TTEE);
    mvaddch (3, 77, ACS_URCORNER);
    mvvline (4, 77, 0, 9);
    mvvline (4, 39, 0, 9);
    mvvline (4, 2, 0, 9);
    mvaddch (13, 2, ACS_LLCORNER);
    hline (0, 74);
    mvaddch (13, 39, ACS_BTEE);
    mvaddch (13, 77, ACS_LRCORNER);
    attrset (A_NORMAL);
    
    mainwin = dupwin (stdscr);
}

static void
curses_create_windows(void)
{
    curses_create_main_window();
}

void
recsetup (void)
{
    register struct tm *Ti;
    clock_t clock_time;

#ifdef CURSES
    initscr ();
    start_color ();

    use_default_colors ();
    assume_default_colors (-1,-1);
    setup_color_pairs ();

    cbreak ();
    echo ();
    keypad (stdscr, TRUE);
    scrollok (stdscr, TRUE);
    curses_create_windows();
#else
    setbuf (stdout, 0);
    setbuf (stdin, 0);
#endif


    time (&clock_time);
    Ti = localtime (&clock_time);
    Today.Yr = Ti->tm_year;
    Today.Mo = Ti->tm_mon + 1;  /* tm_mon starts with 0, we want 1 */
    Today.Da = Ti->tm_mday;

}

/* ********************************** *
 *       Exit routine                 *
 * ********************************** */

int
quitrec (void)
{
#ifdef CURSES
    refresh ();
    endwin ();
#endif /*      CURSES  */

/*   cls;*/
    exit (errno);
}

/* *********************************************** *
 *  Dialogs, warnings, and other popups            *
 * *********************************************** */

void
fil_err (char * mode, char *fil)
{
    WINDOW *errwin;

    errwin = newwin (6, 40, (LINES/2) - 3, (COLS/2) - 20);
    wattrset (errwin, COLOR_SET (COLOR_WHITE, COLOR_RED));
    wclear (errwin);
    box (errwin, 0, 0);
    mvwprintw (errwin, 2, 2, "Cannot %s: %s\nPress [ENTER]...", mode, fil);
    wrefresh (errwin);
    getch ();
    delwin (errwin);
}

void
opn_err (char *fil)
{
    fil_err ("open", fil);
}

void
rd_err (char *fil)
{
    fil_err ("read", fil);
}

void
wrt_err (char *fil)
{
    fil_err ("write", fil);
}

void
clos_err (char *fil)
{
    fil_err ("close", fil);
}
/* ********************************************** *
 *   Utilities   ( originally from recutils.c)    *
 * ********************************************** */

char
yesno (void)
{
    char mykey;

#ifdef CURSES
    refresh ();
#endif

    do
    {
        mykey = (char) getch ();
    }
    while (!strchr ("YN", (mykey = toupper (mykey))));

    return (mykey);
}

#ifndef CURSES                  /*      curses provides getch() */
static char rbuf;

char
getch ()
{
    read (0, &rbuf, 1);
    return rbuf;
}
#endif /*      CURSES  */

void
stripcr (char *line)
{

    register char *cloc = line;

    cloc += (strlen (line) - 1);        /* Point to last character in string */
    if (*cloc == '\x0d')
    {
        *cloc = '\x0';          /* IF CR, replace with null   i.e.           */
    }
}


/* **************************************************************** *
 * "getnstr()" does a readln from stdin, cuts CR from end-of-string *
 *   call: getnstr(string,max-len)                                  *
 *   returns: 0 on error, otherwise ptr to string                   *
 * **************************************************************** */

#ifndef __linux__
char *
getnstr (line, lgth)
     char *line;
     int lgth;
{
    if (!fgets (line, lgth, stdin))
    {
        return 0;
    }

    line[strlen (line)] = '\0';
    return line;
}
#endif

/* ***************************************************************** *
 * center(int path,char *string,int colwidth): centers a string over *
 *     a given space (from the current print position).              *
 * If the string is longer than the line, it will return -1 without  *
 *    printing.                                                      *
 * RETURNS: -1 on error or string length longer than the width.      *
 *                                                                   *
 *  !!!   NOTE:  pthnum is OS9 path #, NOT FILE *   !!!!!            *
 * ***************************************************************** */

int
center (FILE * pthnm, char *str, int wdth, bool toprntr)
{
    register int lsp;           /* # spaces to print on left of string */
    int rsp;                    /* # spaces to print after string      */
    char *spac = " ";

    if (wdth < strlen (str))
        return (-1);
    /* Get left-right space count */
    lsp = (wdth - strlen (str)) / 2;
    rsp = wdth - strlen (str) - lsp;

    while (lsp--)
    {                           /* do left spaces */
#ifdef CURSES
        if (!toprntr)
            addch (' ');
        else
#endif
        if (fwrite (spac, 1, 1, pthnm) == -1)
            return (-1);
    }
#ifdef CURSES
    if (!toprntr)
        printw (str);
    else
#endif
    if (fprintf (pthnm, str) == -1)
        return (-1);
    while (rsp--)
    {                           /* do right spaces */
#ifdef CURSES
        if (!toprntr)
            addch (' ');
        else
#endif
        if (fwrite (spac, 1, 1, pthnm) == -1)
            return (-1);
    }
#ifdef CURSES
    refresh ();
#endif
    return (0);
}
