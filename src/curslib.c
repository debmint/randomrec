/* *********************************************** *
 * curslib.c - custom curses library.  These are   *
 *    utility routines to handle dialogs, etc      *
 * *********************************************** */

/* $Id: curslib.c,v 1.1 2007/01/16 23:55:32 dlb Exp $ */

#include <stdlib.h>
#include <curslib.h>

/* ***************************************************** *
 * setup_colors() initialize a custom set of color pairs *
 *     sets all combos                                   *
 * ***************************************************** */

void
setup_color_pairs (void)
{
    short fg, bg;
    short grp = 0;
    
    for (fg = 0; fg < 8; fg++)
    {
        for (bg = 0; bg < 8; bg++)
        {
            if (grp)
            {
                init_pair (grp, fg, bg);
            }

            ++grp;
        }
    }
}

/* ************************************************ *
 * shadow_box() Create a shadowed box               *
 * Passed: SHADOW_WINDOW *,                         *
 *         y, x of top left of box (window relative *
 *         width, height, in characters             *
 * ************************************************ */

void
shadow_box (SHADOW_WINDOW *wndw, int y_beg, int x_beg, int ysize, int xsize)
{
    attr_t attrs;
    short pair;
    WINDOW *wn = wndw->top_win;
    short bkgnd;

    /* Save window attributes to restore before returning */
    
    wattr_get (wn, &attrs, &pair, NULL);
    bkgnd = pair % 8;

    if (bkgnd > 7)
    {
        wprintw (wn, "Error! background is not is range: %d\n", bkgnd);
        wrefresh (wn);
        return;
    }
  
    /* Do white lines */
    
    wattrset (wn, A_BOLD | COLOR_SET (COLOR_WHITE, bkgnd));
    mvwaddch (wn, y_beg, x_beg, ACS_ULCORNER);
    whline (wn, 0, xsize - 2);
    mvwvline (wn, y_beg + 1, x_beg, 0, ysize -2);
    mvwaddch (wn, y_beg + ysize - 1, x_beg, ACS_LLCORNER);

    /* Shadowed lines now */

    wattron (wn, COLOR_SET (COLOR_BLACK, bkgnd));
    whline (wn, 0, xsize - 2);
    mvwvline (wn, y_beg + 1, x_beg + xsize - 1, 0, ysize - 2);
    mvwaddch (wn, y_beg, x_beg + xsize - 1, ACS_URCORNER);
    mvwaddch (wn, y_beg + ysize - 1, x_beg + xsize - 1, ACS_LRCORNER);
    wrefresh (wn);
    

    /* Restore attributes to what they were upon entry */
    wattr_set (wn, attrs, pair, NULL);
}
    
/* ************************************************** *
 * clear_window ()  clears a window to the background *
 *      color                                         *
 * Passed : SHADOW_WINDOW *                           *
 *          y size, x size, fg color, bg color        *
 *          TRUE if shadow wanted, false if not       *
 * Returns: nothing, but sets COLOR_PAIR to passed    *
 *          parameters                                *
 * ************************************************** */

void
clear_window (WINDOW *wndw, int ysize, int xsize, short fg, short bg)
{
    int ypos,
        xpos = 0;
    
    wattrset (wndw, COLOR_SET (fg, bg));

    for (ypos = 0; ypos < ysize; ypos++)
    {
        wmove (wndw, ypos, 0);
        
        for (xpos = 0; xpos < xsize; xpos++)
        {
            waddch (wndw, ' ');
        }
    }
    
    wrefresh (wndw);
}

/* *************************************************** *
 * create_bordered_window_cleared()    Create a new    *
 *    window cleared to background color               *
 * Passed : newwin parameters, in same order           *
 *          foreground color, background color         *
 * Returns: WINDOW * to new window,                    *
 *          COLOR_PAIR to passed pair                  *
 * *************************************************** */

SHADOW_WINDOW *
create_window_cleared (int ysize, int xsize, int ytop, int xleft,
                       short fgcolor, short bgcolor, bool shadowed)
{
    SHADOW_WINDOW *wndw;

    wndw = (SHADOW_WINDOW *) calloc (1, sizeof(SHADOW_WINDOW));
    
    if (shadowed)
    {
        wndw->shadow_win = newwin (ysize, xsize, ytop + 1, xleft + 1);
        clear_window (wndw->shadow_win, ytop + 1, xleft + 1,
                      COLOR_WHITE, COLOR_BLACK);
    }
    
    wndw->top_win = newwin (ysize, xsize, ytop, xleft);

    clear_window (wndw->top_win, ysize, xsize, fgcolor, bgcolor);

    return wndw;
}

/* ************************************************* *
 * end_shadow_win() - ends the window, its shadow,   *
 *      and frees the SHADOW_WINDOW structure memory *
 * Passed: the SHADOW_WINDOW pointer                 *
 * ************************************************* */

void
end_shadow_win (SHADOW_WINDOW *win)
{
    if (!win)  /* Avoid possible segfault */
    {
        return;
    }
    
    if (win->shadow_win)   /* Shadow window? */
    {
        delwin (win->shadow_win);
    }

    if (win->top_win)   /* should be one of these, at least */
    {
        delwin (win->top_win);
    }

    /* Free memory allocated by the SHADOW_WINDOW structure */
    free (win);
}

/* **************************************************** *
 * wr_button_next() - selects next item in the ring    *
 *    skipping any items not a button (currently        *
 *    flagged with a NULL "wr_label" pointer            *
 * Passed:   WINDOW * to menu's base window             *
 *           ring (pointer to ring's "first" element)   *
 *           ** current ring item selected              *
 * **************************************************** */

void
wr_button_next (WDGT_RING **current)
{
    /* avoid an endless loop in case there aren't at least 2 buttons */
    if ( (*current)->wr_next  != (*current)->wr_prev)
    {
        do {
            widget_ring_next (current);
        } while (!(*current)->wr_label);
    }
}

/* ****************************************************** *
 * wr_button_prev() - See description of wr_button_right  *
 * ****************************************************** */

void
wr_button_prev (WDGT_RING **current)
{
    /* avoid an endless loop in case there aren't at least 2 buttons */
    if ( (*current)->wr_next  != (*current)->wr_prev)
    {
        do {
            widget_ring_prev (current);
        } while (!(*current)->wr_label);
    }
}

/* ****************************************************** *
 * wdg_menu_run - run a menu until some selection is made *
 * ****************************************************** */

int
wdg_menu_run (MENU *mnu, WDGT_RING **cur_ring,
              int mcols)
{
    int ch;
    
    /* Set up some variables to save dereferencing so much */
    WINDOW *wndw = (*cur_ring)->wr_win;
    WDGT_RING *ring = (*cur_ring)->wr_first;

    while (1)
    {
        bool done = FALSE;

        switch (ch = wgetch (wndw))
        {
            case '\x0a':
            case '\x0d':
                done = TRUE;
                break;
            case '\x09':
                widget_ring_next (cur_ring);
                break;
            case KEY_DOWN:
                if (*cur_ring == ring)
                {
                    menu_driver (mnu, REQ_DOWN_ITEM);
                }
                else
                {
                    wr_button_next (cur_ring);
                }
                break;
            case KEY_UP:
                if (*cur_ring == ring)
                {
                    menu_driver (mnu, REQ_UP_ITEM);
                }
                else
                {
                    wr_button_prev (cur_ring);
                }
                break;
            case KEY_RIGHT:
                if (*cur_ring == ring)
                {
                    /*if (mcols > 1)*/
                    {
                        menu_driver (mnu, REQ_RIGHT_ITEM);
                    }
                }
                else
                {
                    wr_button_next (cur_ring);
                }
                break;
            case KEY_LEFT:
                if (*cur_ring == ring)
                {
                    if (mcols > 1)
                    {
                        menu_driver (mnu, REQ_LEFT_ITEM);
                    }
                }
                else
                {
                    wr_button_prev (cur_ring);
                }
                break;
            default:
                break;
        }   /* end switch (ch = wgetch()) */

        if (done)
        {
            if ((*cur_ring)->wr_retval != -1)
            {
                return (item_index (current_item (mnu)));
            }
            else
            {
                return 0;
            }
        }
    }
}
/* temporarily include the other files to save so much makefile stuff
 * may later compile them separately if I develop it as a library
 */

#include "wdgdialogs.c"
#include "widgetring.c"
