/* ****************************************** *
 * loadjobs.c : handles loading of data files *
 * used by "record"                           */

 /* $Id$ */

#include "record.h"

#ifdef __linux__
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>
#include <unistd.h>
#define _READ  O_RDONLY
#endif

#ifdef CURSES
#define printf printw
#endif

char ThisYr;

#ifdef linux

long
_gs_size (FILE * fp)
{
    int sz;

    fseek (fp, 0, SEEK_END);
    sz = ftell (fp);
    rewind (fp);

    return (long) sz;
}
#endif

/* *************************************************************** *
 * loadjobs()   on initialization, reads job directory into memory *
 *  Returns:  count of jobs loaded on success   -1 on read error   *
 * *************************************************************** */

int
loadjobs (struct job_index *jobstr)
{
    register int jobcnt = 0, n;
    do
    {
        if ((n = (fscanf (fpath, "Job=%s Dir=%s\n", jobstr->JobNam,
                                                    jobstr->JobDir))) < 2)
        {
            if (feof (fpath))
            {
                clearerr (fpath);
                return jobcnt;
            }
            else
            {
                rd_err (stuff);
                return ERR;
            }
        }
        else
        {
            register char *ndx;
            while ((ndx = index (jobstr->JobNam, '~')))
            {
                *ndx = ' ';
            }
            ++jobstr;
        }
    }
    while (++jobcnt < MaxJob);
    return jobcnt;
}

  /* ***************************************************** *
   * pikacct()   Select a job from job directory and       *
   *             load appropriate related data into memory *
   * ***************************************************** */

struct job_index *
pikacct (struct job_index *jobpik)
{
    register int jchoic;
    char chbuf[4];
    register struct job_index *jbpt = jobpik;

    if (!strlen (jbpt->JobNam))
    {
        printf ("No Jobs in Job Directory\n");
        printf ("Press [ENTER]");
        getch ();
        /*return (0);*/
    }

    do
    {
        cls;
        
        jbpt = jobpik;

        /* Print a choice to abort */
        
        printf( "\n%02d : %s\n\n", 0, "Abort Job choice");
        
        for (jchoic = 0; jchoic < NumJobs; jbpt++)
        {
            if (strlen (jbpt->JobNam))
            {
                printf ("%02d : %s\n", ++jchoic, jbpt->JobNam);
            }
        }

        printf ("\n I : %s\n", "Initialize a record");
    
        printf ("\nEnter Desired Job # <1> to <%d>->> ", NumJobs);
        getnstr (chbuf, sizeof (chbuf));
        refresh();

        /* Abort if '0' is pressed */

        if ((*chbuf == 'I') || (*chbuf == 'i'))
        {
            recinit = TRUE;
            reciniz();
            recinit = FALSE;
        }
        
        if (*chbuf == '0')
        {
            return 0;
        }
    }
    while (((jchoic = atoi (chbuf)) < 1) || (jchoic > NumJobs));
    
    jbpt = jobpik;              /* Reset to base */
    jbpt += (--jchoic);
    return (jbpt);
}

/* ************************************************************ *
 * in_ex() Loads Income & Expense data for job into memory      *
 *  strsiz AND readsiz are both passed for updating outdated    *
 *  file versions                                               *
 * ************************************************************ */

static int
in_ex (void *cgstr, char *fil, int maxnum, int strsiz, int readsiz)
{
    char *cg0 = cgstr;          /* Save cgstr (base of structure) */
    int count;

    errno = 0;                  /* Be sure errno is null       */
    if (!(fpath = fopen (makfil (tmpstr, fil), "r")))
    {
        opn_err (tmpstr);
        return ERR;
    }
    memset (cgstr, 0, maxnum * strsiz);
    count = 0;                  /* Reset cgstr to base of structure */
   
    while (fread (cg0, readsiz, 1, fpath) == 1)
    {
        if (++count > maxnum)
        {
            fclose (fpath);
            return ERR;
        }
        cg0 += strsiz;
    }
    
    if (feof (fpath))
    {
        errno = 0;              /* EOF not a true error here */
    }
    
    fclose (fpath);
    
    if (errno)
    {
        return (ERR);
    }
    else
    {
        return (count);
    }
}

     /* We now have the job choice, now load its data */

int
loaddat (void)
{
    long fsiz;
    register FILE *fp;

    /* First get size of data file to get NumRecs */

    if (!(fp = fopen (makfil (tmpstr, datafil), "rb")))
    {
        opn_err (tmpstr);
        return ERR;
    }
   
    if ((fsiz = _gs_size (fp)) == ERR)
    {
        fclose (fp);
        fprintf (stderr, "Error getting size of data file\n");
        return ERR;
    }
    else
    {
        LstRec = (int) (fsiz / (long) sizeof (Rec));
        fclose (fp);
    }
    
    /* Get year totals */
    if ((path = open (makfil (tmpstr, ttlfil), _READ)) == ERR)
    {
        opn_err (tmpstr);
        return ERR;
    }
   
    if (read (path, &YrTtl, sizeof (YrTtl)) == ERR)
    {
        rd_err (tmpstr);
        close (path);
        return ERR;
    }
    
    close (path);

    /* Now Income/Expense categories */
    if ((InCgyNum = in_ex (&Income, incmfil, MaxInc,
                           sizeof (Income[0]), sizeof (Income[0]))) == ERR)
    {
        fprintf (stderr, "Error getting income cgys\n");
        return ERR;
    }

    if ((ExCgyNum = in_ex (&Expense, expfil, MaxExp,
                           sizeof (Expense[0]), sizeof (Expense[0]))) == ERR)
    {
        fprintf (stderr, "Error getting expense cgys\n");
        return ERR;
    }

    /* ...and Payor data */
    if ((NumPayrs = in_ex (&Person, payfil, MaxPers,
                           sizeof (Person[0]), sizeof (Person[0]))) == ERR)
    {
        fprintf (stderr, "Error getting Payor/Payee cgys\n");
        return ERR;
    }

    return 0;   /* Never comes here??? */
}

   /* ********************************************************** *
    * makfil(dest.str, src.str)   build a filename for file open *
    *    filename = [directory path] + filename + Year           *
    *    returns: ptr to fil                                     *
    * ********************************************************** */

char *
makfil (char *fil, char *str)
{
    /* If a pathname is specified, copy it */
    strcat (strcpy (fil, fildir), str);
    return (fil);
}

   /* *********************************************************** *
    * fwrt: a subroutine for writing a single entry to either     *
    *    income, expense, or payor file.  Used in "record".       *
    *    For "recinit", it will be a null subroutine, as          *
    *    recinit will wait and write the entire file at one time. *
    * *********************************************************** */

int
fwrt (char *fnam, char *adres, int strsiz)
{

    /*  If in "record", write this one, else if in "recinit", skip */

    if (!recinit)
    {
        if (!(fpath = fopen (fnam, "ab")))
        {
            fprintf (stderr,
                  "Error in file open: (%s)\nAborting write... Press [ENTER]...",
                 fnam);
            getch ();
            return ERR;
        }

        if (fwrite (adres, strsiz, 1, fpath) != 1)
        {
            fprintf (stderr,
                 "Error om file write: (%s)\n Aborting write... Press [ENTER]...",
                 fnam);
            getch ();
            fclose (fpath);
            return ERR;
        }
        /*      If "fclose()" is unsuccessful, let it go for now... the only
           problem would be running out of available paths   */
        fclose (fpath);
    }

    return 0; /* to pacify gcc so it won't squawk */
}

   /* ******************************************************* *
    * NOTE: The following routines write totals data to files *
    * ******************************************************* */

int
updatfil (void)
{

    printf ("\nUpdating totals files...  Please wait...\n");
    /* Save year totals */
    printw ("Writing %s...\n", makfil (tmpstr, ttlfil));
    refresh ();

    if (!(fpath = fopen (tmpstr, "wb")))
    {
        opn_err (tmpstr);
        return ERR;
    }

    YrTtl.LstUpdat = Today;

    if (fwrite (&YrTtl, sizeof (YrTtl), 1, fpath) != 1)
    {
        wrt_err (tmpstr);
        fclose (fpath);
        return ERR;
    }

    fclose (fpath);

    /* Save payor/payee file */
    printw ("Writing %s...\n", makfil (tmpstr, payfil));
    refresh ();

    if (NumPayrs)
    {
        if (!(fpath = fopen (tmpstr, "w")))
        {
            opn_err (tmpstr);
            return ERR;
        }

        if (fwrite (Person, sizeof (Person[0]), NumPayrs, fpath) != NumPayrs)
        {
            wrt_err (tmpstr);
            fclose (fpath);
            return ERR;
        }
        fclose (fpath);
    }
    else
    {
        if (!(fpath = fopen (tmpstr, "a")))
        {
            opn_err (tmpstr);
            return ERR;
        }

        fclose (fpath);
    }
    /* Now Income categories */
    printw ("Writing %s...\n", makfil (tmpstr, incmfil));
    refresh ();

    if (InCgyNum)
    {
        if (!(fpath = fopen (tmpstr, "w")))
        {
            opn_err (tmpstr);
            return ERR;
        }

        if (fwrite (Income, sizeof (Income[0]), InCgyNum, fpath) != InCgyNum)
        {
            wrt_err (tmpstr);
            fclose (fpath);
            return ERR;
        }
        fclose (fpath);
    }
    else
    {
        if (!(fpath = fopen (tmpstr, "a")))
        {
            opn_err (tmpstr);
            return ERR;
        }
        fclose (fpath);
    }

    /*  ... and Expense cgys */
    printw ("Writing %s...\n", makfil (tmpstr, expfil));
    refresh ();

    if (ExCgyNum)
    {
        if (!(fpath = fopen (tmpstr, "w")))
        {
            opn_err (tmpstr);
            return ERR;
        }

        if (fwrite (Expense, sizeof (Expense[0]), ExCgyNum, fpath) !=
            ExCgyNum)
        {
            wrt_err (tmpstr);
            return ERR;
        }
        fclose (fpath);
    }
    else
    {
        if (!(fpath = fopen (tmpstr, "a")))
        {
            opn_err (tmpstr);
            return ERR;
        }
        fclose (fpath);
    }

    UpToDate = TRUE;            /* If we get here, files written */
    return 0;                   /* return 0 on success, -1 on any error */
}

