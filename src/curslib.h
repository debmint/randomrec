/* curslib.h - definitions for my custom curses library */

#ifndef GOT_CURSLIB
#  define GOT_CURSLIB

#include <menu.h>
#include <form.h>
#include <string.h>

/* Color-pair sets */
#define COLOR_SET(f,b) COLOR_PAIR((f*8) + b)

/* Std Colors */
#define BUT_SEL COLOR_SET (COLOR_RED, COLOR_YELLOW) | A_UNDERLINE
#define BUT_UNSEL COLOR_SET (COLOR_BLACK, COLOR_WHITE) | A_DIM | A_UNDERLINE

/* Dialog return values */
#define WDG_RET_CANCEL -1
#define WDG_RET_NULL 0
#define WDG_RET_OK 1

/* ************************************************************ *
 * struct WDGT_RING - a dialog will be created where it points  *
 *    to an endless "ring" of widgets.  The dialog's structure  *
 *    will point to the currently-selected widget. Tabbing will *
 *    move you around the circle to the next/previous widget    *
 * ************************************************************ */

typedef struct widgring {
    WINDOW *wr_win;              /* Keep WINDOW * in all. */
    struct widgring *wr_prev,
                    *wr_next,
                    *wr_first;   /* Keep first (usually form etc) in all */
    int wr_y,                    /* Y pos of widget */
        wr_x;                    /* X pos of widget */
    char *wr_label;       /* ptr to widget label.  maybe NULL = form,etc? */
    int wr_retval;        /* a value for that button to return when pressed */
} WDGT_RING;

/* *************************************** *
 * struct SHADOW_WINDOW - a widow with its *
 *          shadow                         *
 * *************************************** */

typedef struct
{
    WINDOW *top_win,
           *shadow_win;
} SHADOW_WINDOW;

#include "libproto.h"
#endif  /* !GOT_CURSLIB */
