/* datcvt - convert the old-style char-sized date structure to int-sized storage*/

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>

#define MAIN

#include "record.h"

typedef struct {
    char y,m,d;
} CDAT;

struct oldyttl {
    char RRev;   /* This should be "int" for svn versions later than r22 */
    CDAT LstUpdat;
    char res[30];
    float TotXpns,TotInc;   /* This should be "int" for svn -r >= 22 */
} OldYrTtl;

struct oldtrans {
    CDAT    Date;
    char    Descr[33];
    char    Quan[10];
    char    Invoic[7];
    char    ChkNo[6];
    char    ToFrm,
            Cgy,
            IsIncom;
    float   Cost;
} OldRec;

struct old_pp
{
    char   Who[23];
    float  GotFrom,
           PdTo;
} OldPP[48];

struct oldcgystr {
       char     CgyNam[23];     /* Category name                        */
       float    CgyTot; /* Total for category                           */
       char     DscDf[33],      /* Non-empty string if default          */
                QtyDf,          /* 0=no default -1=skil                 */
                InvDf,          /* 0=no default -1=skip                 */
                ChkDf,          /* 0=no default -1=skip                 */
                TfDf;           /* 0=no dflt, -1=skip, or Tofrom+1      */
} OldInEx[30];

void
fixinex (char *fname)
{
    int cgnum = 0;
    struct oldcgystr *ccgy = OldInEx;
    FILE *fp;

    if ( ! (fp = fopen (fname, "r")))
    {
        fprintf(stderr, "Cannot open %s for reading\n", fname);
        exit (errno);
    }

    while (fread (ccgy, sizeof (struct oldcgystr), 1, fp))
    {
        ++cgnum;
        ++ccgy;
    }

    if ( ! feof (fp))
    {
        fprintf (stderr, "!!!!!!!!Only read %d records from %s\n", cgnum,
                fname);
    }

    fclose (fp);    /* Close it sio we can truncate it */

    if ( ! (fp = fopen (fname, "w")))
    {
        fprintf (stderr, "Cannot open %s for writing\n", fname);
        exit (errno);
    }

    ccgy = OldInEx;

    while (cgnum--)
    {
        /* We'll just use Income[0] for the new data */
        strcpy (Income[0].CgyNam, ccgy->CgyNam);
        Income[0].CgyTot = ccgy->CgyTot;
        strcpy (Income[0].DscDf, ccgy->DscDf);
        Income[0].QtyDf = ccgy->QtyDf;
        Income[0].InvDf = ccgy->InvDf;
        Income[0].ChkDf = ccgy->ChkDf;
        Income[0].TfDf = ccgy->TfDf;

        if ( ! fwrite (Income, sizeof (Income[0]), 1, fp))
        {
            fprintf (stderr, "Error writing to Inex File %s... Aborting\n",
                    fname);
            exit (errno);
        }

        ++ccgy;
    }

    fclose (fp);

    fprintf (stderr, "%s rewritten successfully\n", fname);
}

void fixdata (char *dir)
{
    FILE *infile, *outfile;
    int count;
    struct old_pp *pp_ptr;

    printf ("\nFixing Dir %s...\n", dir);

    if (chdir (dir)) {
        fprintf(stderr,"Cannot chdir to %s\n", dir);
        exit (errno);
    }

    if (rename ("datafil.rec","datafil.rec~")) {
        fprintf (stderr, "Cannot rename file\n");
        exit (errno);
    }

    if (!(infile=fopen("datafil.rec~", "r"))) {
        fprintf(stderr, "Cannot open file for read\n");
        exit(errno);
    }

    if (!(outfile=fopen("datafil.rec", "w"))) {
        fprintf(stderr, "Cannot create file for write\n");
        exit(errno);
    }

    while ( fread (&OldRec, sizeof(OldRec), 1, infile)) {
        Rec.Date.Yr = OldRec.Date.y;
        Rec.Date.Mo = OldRec.Date.m;
        Rec.Date.Da = OldRec.Date.d;
        strncpy (Rec.Descr, OldRec.Descr, sizeof(Rec.Descr));
        strncpy (Rec.Quan, OldRec.Quan, sizeof(Rec.Quan));
        strncpy (Rec.Invoic, OldRec.Invoic, sizeof(Rec.Invoic));
        strncpy (Rec.ChkNo, OldRec.ChkNo, sizeof(Rec.ChkNo));
        Rec.ToFrm   = OldRec.ToFrm;
        Rec.Cgy   = OldRec.Cgy;
        Rec.IsIncom   = OldRec.IsIncom;
        Rec.Cost   = OldRec.Cost;

        fwrite (&Rec, sizeof (Rec), 1, outfile);
    }
    
    fclose (infile);
    fclose (outfile);

    printf ("'datafile.rec' written successfully\n");

    if (!(infile=fopen("yr_ttl.rec", "r+"))) {
        fprintf(stderr, "Cannot open yrttls\n");
        exit(errno);
    }

    fread (&OldYrTtl, sizeof(OldYrTtl), 1, infile);
    YrTtl.RRev = 4;
    YrTtl.LstUpdat.Yr = OldYrTtl.LstUpdat.y + 1900;
    YrTtl.LstUpdat.Mo = OldYrTtl.LstUpdat.m;
    YrTtl.LstUpdat.Da = OldYrTtl.LstUpdat.d;
    memset (YrTtl.res, 0, 30);
    YrTtl.TotXpns = OldYrTtl.TotXpns;
    YrTtl.TotInc  = OldYrTtl.TotXpns;
    
    rewind (infile);
    fwrite (&YrTtl, sizeof(YrTtl), 1, infile);
    fclose (infile);

    /* Do the income and expense stuff */
    fixinex ("incom.rec");
    fixinex ("expns.rec");

    /* The payors/payees */

    if ( ! (infile = fopen ("payors.rec", "r+")))
    {
        fprintf (stderr, "Cannot open 'payors.rec' for read\n");
        exit (errno);
    }

    count = 0;
    pp_ptr = OldPP;

    while (fread (pp_ptr, sizeof (struct old_pp), 1, infile))
    {
        ++count;
        ++pp_ptr;
    }

    rewind (infile);    /* We can do this, since the field size is bigger */
    pp_ptr = OldPP;

    while (count--)
    {
        struct pay_p *newpyr = &Person[0];

        memset (newpyr, 0, sizeof(struct pay_p));
        strcpy (newpyr->Who, pp_ptr->Who);
        newpyr->GotFrom = pp_ptr->GotFrom;
        newpyr->PdTo = pp_ptr->PdTo;
        
        if ( ! fwrite (newpyr, sizeof (struct pay_p), 1, infile))
        {
            fprintf (stderr, "!!! ERROR writing to 'payors.rec... aborting\n");
            exit (errno);
        }

        ++pp_ptr;
    }

    fclose (infile);

    /* Done.. now return to home directory */
    
    if (chdir("..")) {
        fprintf (stderr, "Cannot return to base directory");
        exit (errno);
    }
}

int main(int argc, char **argv)
{
    DIR *dot;
    struct dirent *dirlist;

    if (!(dot = opendir ("."))) {
        fprintf(stderr, "FATAL, cannot open \".\"\n");
        exit (1);
    }

    while (dirlist = readdir (dot)) {
        if ((dirlist->d_type == DT_DIR) &&
                        !strncmp("RECORD", dirlist->d_name, 6)) {
            fixdata(dirlist->d_name);
        }
    }

    closedir (dot);
}

