/* *************************************************** *
 * recinit.c : Program to initialize a record database *
 * *************************************************** */

/* $Id$ */

#include "record.h"

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <strings.h>
#include <ctype.h>

int DoRplac = FALSE,
              tmpyr;
char tbuf[80];


   /* ************************************ *
    * namejob(): Routine to input job name *
    *   returns: nothing                   *
    * ************************************ */


static void
namejob (void)
{
    register int cnt;

    /* Repeat till we get a non-null string */
    do
    {
        do
        {
            printw ("\nEnter a NAME for this new job..> ");
            getnstr (tbuf, sizeof (tbuf));
        }
        while (!strlen (tbuf) || (strlen (tbuf) >= sizeof (Job[0].JobNam)));

        /* and check to see if it already is here */
        cnt = 0;
        do
        {
            if (!strcasecmp (tbuf, Job[cnt++].JobNam))
            {                   /* If it matches */
                tbuf[0] = '\x0';        /* Tbuf = "" to flag a match   */
            }
        }
        while (tbuf[0] && (cnt < NumJobs));

        if (!tbuf[0])
        {
            printw ("The name %s has already been used...\n",
                    Job[cnt - 1].JobNam);
            printw ("  *** REDO ***\n");
        }
    }
    while (!tbuf[0]);

    jobptr = (struct job_index *) tbuf;
}

static void
cgd1 (char *c1, int cgnum, int top_, int strsz)
{
    char *c2;            /* "get" category ptr ( c1 will be "put" ptr */

    c2 = c1;             /* c2 points to next category after c1 */
    c2 += strsz;
    top_ -= cgnum;       /* top_ equals count of cgy's above c1 (to delete) */

    while (--top_)
    {
        memcpy (c1, c2, strsz); /* move structure down one */
        c1 = c2;                /* Move c1 to c2 spot */
        c2 += strsz;            /* and c2 -> next   */
    }

    memset (c1, 0, strsz);      /* Null out last element */
}

        /* ************************************************* *
         * cgdel(): Chance to delete any elements not wanted *
         * for now, return 1 on success, 0 on failure,       *
         * although we aren't using it.. maybe change to     *
         * void function later.                              *
         * ************************************************* */


static int
cgdel (void)
{
    char dbuf[10];
    int cgn, maxnum = InCgyNum + ExCgyNum;

    dbuf[0] = '\0';

    for (;;)
    {
        if (!maxnum)
            return (0);
        cgyshow ();
        CurXY (scrn, 2, 22);
        printw ("Enter # to delete... 0 when done...");
        do
        {
#ifdef CURSES
            getnstr (dbuf, sizeof (dbuf));
            cgn = atoi (dbuf);
#else
            cgn = atoi (fgets (dbuf, sizeof (dbuf), stdin));
#endif

            /* Don't delete either "Misc." field   */

            /* Make "0" entry uniform for all platforms */
            if (!strcmp (dbuf, "0\n"))
                strcpy (dbuf, "0");

            if (cgn == InCgyNum + 1)
                cgn = 0;        /* Don't delete Expense "Misc." */
            if (!strcmp (dbuf, "0"))
                return (0);     /* Zero entered, return */
        }
        while ((cgn > maxnum) || (cgn < 2));    /* cgn can't be 1 (Misc.) */

        --cgn;                  /* Change cgn to begin point to [0] element */

        if (cgn < InCgyNum)
        {                       /* We're dealing with Income item */
            cgd1 (Income[cgn].CgyNam, cgn, InCgyNum--, sizeof (Income[0]));
        }
        else
        {
            cgn -= InCgyNum;
            cgd1 (Expense[cgn].CgyNam, cgn, ExCgyNum--, sizeof (Expense[0]));
        }

        --maxnum;
    }

    return 1;
}

        /* ************************************************** *
         * paydel(): Chance to delete any elements not wanted *
         * for now, return 1 on success, 0 on failure,        *
         * although we aren't using it.. maybe change to      *
         * void function later.                               *
         * ************************************************** */


static int
paydel (void)
{
    char dbuf[10];
    int payn, maxnum = NumPayrs;

    dbuf[0] = '\0';

    for (;;)
    {
        if (!maxnum)
        {
            return (0);
        }

        pyrshow ();
        CurXY (scrn, 2, 22);
        printw ("Enter # to delete... 0 when done...");

        do
        {
#ifdef CURSES
            getnstr (dbuf, sizeof (dbuf));
            payn = atoi (dbuf);
#else
            payn = atoi (fgets (dbuf, sizeof (dbuf), stdin));
#endif

            /* Make "0" entry uniform for all platforms */
            if (!strcmp (dbuf, "0\n"))
                strcpy (dbuf, "0");

            if (!strcmp (dbuf, "0"))
                return (0);     /* Zero entered, return */
        }
        while ((payn > maxnum) || (payn < 2));

        --payn;                 /* Change cgn to begin point to [0] element */
        cgd1 (Person[payn].Who, payn, NumPayrs--, sizeof (Person[0]));
        --maxnum;
    }

    return 1;
}


static void
PutYr (char *fnam)
{
    if (fprintf (fpath, "Yr=%d\n", RecYr + 1900) == -1)
    {
        wrt_err (fnam);
    }
}


static void
nostuff (char *pmpt)
{
    printw ("\n%s\n", pmpt);
    printw ("\"stuff\" file won't show this entry\n");
    printw ("Delete the directory \"jobptr->JobDir\"\n");
    printw ("You might try to add this entry again\n");
    printw ("***Hit a key***\n");
    getch ();
    quitrec ();
}

int
reciniz (void)
{
    register char *ndx;
    int count, tmpnum;
    struct job_index OldJob[MaxJob]; /* Duplicate data structure for loading */

    cls;

    /* Null out All elements of Person structure */

    memset (Person, 0, sizeof (Person));

    for (;;)
    {
        if (RecYr)
        {
            tmpyr = RecYr;
            break;
        }
        
        printw ("\nEnter a 2-digit value for Year of Record ..\n");
        printw (" 70-99 = 20th Cent || 00 - 69 = 21st Cent .. > ");
#ifdef CURSES
        getnstr (tbuf, sizeof (tbuf));
        tmpnum = atoi (tbuf);
#else
        fflush (stdout);
        tmpnum = atoi (fgets (tbuf, sizeof (tbuf), stdin));
#endif
        if (tmpnum < 70)
        {
            tmpnum += 2000;
        }
        else
        {
            if (tmpnum < 100)
            {
                tmpnum += 1900;
            }
        }

        if ((tmpnum > 1969) && (tmpnum < 2070))
        {
            printw ("\n\nYear of Record = %d\n Correct ? < Y/N >", tmpnum);
#ifndef CURSES
            fflush (stdout);
#endif
            if (yesno () == 'Y')
            {
                tmpyr = tmpnum - 1900;
                break;
            }
        }
    }

    /* Test if recstuff already exists */
    if (!(fpath = fopen (stuff, "r+")))
    {
        if ((errno == ENOENT))
        {                       /*   If file does not exist */
            errno = 0;          /*   Clear error            */

            if (!(fpath = fopen (stuff, "w+")))
            {                   /* if can't create it */
                printw ("Cannot create the file: %d\n", stuff);
                refresh ();
                quitrec ();
            }
        }
        else
        {
            opn_err (stuff);
        }
    }

    rewind (fpath);             /* Be sure we're at begin of file */

    if ((fscanf (fpath, "Yr=%d\n", &count)) == -1)
    {                           /* If can't read one byte (RecYr) */
        if (feof (fpath))
        {                       /* If eof(no data....    */
            clearerr (fpath);
            RecYr = tmpyr;
            PutYr (stuff);      /* then write RecYr      */
        }
        else
        {
            printw ("Can't read RecYr from recstuff... Aborting...\n");

            quitrec ();
        }
    }
    else
    {                           /* We did get RecYRr */
        if (count != tmpyr + 1900)
        {                       /* RecYr does not match */
            clear ();
            printw ("\nCurrent \"recstuff\" is for the year %d\n",
                    (int) tbuf[0]);
            printw ("Your entry for Record Year is %d\n", tmpyr);
            printw ("\nYour choice of action toward \"recstuff\"...\n");
            printw ("<D>elete & replace,,, <R>ename,,, <Q>uit ?? ");

            do
            {
                if ((*tbuf = (char) getch () == ERR))
                {
                    quitrec ();
                }
            }
            while (!strchr ("DdRrQq", tbuf[0]));

            switch (toupper (tbuf[0]))
            {
            case 'Q':
                fclose (fpath); /* Not needed, really */
                quitrec ();     /* May as well quit   */
            case 'R':
                break;          /* Fix later */
            default:
                fclose (fpath); /* Close it  ..... */

                if (!(fpath = fopen (stuff, "w+")))
                {
                    opn_err (stuff);
                }

                RecYr = tmpyr;
                PutYr (stuff);
                break;
            }
        }                       /* End : if(tbuf[0] != RecYr */
    }

    if ((NumJobs = loadjobs (Job)) == -1)
    {
        rd_err (tbuf);
        fclose (fpath);
        quitrec ();
    }

    if (fclose (fpath) == EOF)
    {
        clos_err (stuff);
        quitrec ();
    }

    if (NumJobs == MaxJob)
    {
        printw
            ("Maximum number of Jobs has already\n been entered... Aborting 'recinit'\n");
        quitrec ();
    }

    tmpnum = NumJobs;  /* tmp save NumJobs for if load from existing dir */

    /* Select source directory for data or select manual input */
    fildir[0] = '\0';

    do
    {
        if (strlen (fildir)) {
            printw ("\nCannot access %s\n", fildir);
        }

        printw ("\nIf you have an existing record from which\n");
        printw ("you wish to get your data, Enter a directory path\n");
        printw ("from which you wish to obtain this data\n");
        printw ("Press <ENTER> for manual entry..> ");
        getnstr (fildir, sizeof (fildir) - 1);
        
        if (!strlen (fildir))
        {
            break;
        }
    }
    while (access (fildir, (R_OK | F_OK)));

    printw ("\n");

    /* If a directory was selected, load data; */

    if (strlen (fildir))
    {
        if (fildir[strlen (fildir) -1] != '/')
        {
            strcat (fildir, "/");   /* Add slash to end of pathname */
        }
        
        if (!(fpath = fopen (strcat (strcpy (tbuf, fildir), stuff), "r+")))
        {
            printw ("Cannot access %s\nAborting 'recinit'\n", tbuf);
            quitrec ();
        }

        /* Read year from old file and discard */

        if (fscanf (fpath, "%s\n", tmpstr) < 1)
        {
            printw ("..Aborting... Cannot get Record year from %s\n", tbuf);
            fclose (fpath);
            quitrec ();
        }

        if ((NumJobs = loadjobs (OldJob)) == -1)
        {
            rd_err (tbuf);
            fclose (fpath);
            quitrec ();
        }
        if (fclose (fpath) == -1)
        {
            printw ("Couldn't close: %s\nProceeding anyhow\n\n", tbuf);
        }

        if (!(jobptr = pikacct (OldJob)))
        {
            namejob ();         /* No job found by pikacct, go input one */
        }
        else
        {                       /* else load related data                */
            strcat (strcat (fildir, jobptr->JobDir), "/");
            if (loaddat () == -1)
                quitrec ();
            cgdel ();           /* Go delete some cgy's if desired */
            paydel ();          /* and also payors   */
        }
    }
    else
    {
        namejob ();
        /* The first element  of Persons & Inc/Exp.is "Misc."   */
        NumPayrs = 1;
        strcpy (Person[0].Who, "Misc.");
        Person[0].GotFrom = Person[0].PdTo = 0;

        InCgyNum = ExCgyNum = 1;
        strcpy (Expense[0].CgyNam, strcpy (Income[0].CgyNam, "Misc."));
        Expense[0].CgyTot = Income[0].CgyTot = 0;
    }

    /* Now we have arrived here with any carryovers from copied database
     * or either an empty set of structures
     */
    /* Now enter new job to jobdir */

    /* We now have a job name, ptd to by "jobptr".  In either case above,
     * the Jobdir has to be defined for this routine.
     */
    
    NumJobs = tmpnum;           /* Put this-yr NumJobs back in place */
    
    /* Clear out fildir, no longer needed */
   
    memset (fildir, 0, sizeof (fildir));
    RecYr = tmpyr;
    strcpy (Job[NumJobs].JobNam, jobptr->JobNam);
    jobptr = &Job[NumJobs];     /* Now use ptr for simplicity */
    sprintf ((jobptr->JobDir), "RECORD%03d.%d", NumJobs, RecYr);
    ++NumJobs;

    /* Now add any desired categories */
    while ((tmpnum = addcgy ()))
    {
        if (tmpnum == -1)
        {                       /* If error */
            quitrec ();
        }
    }

#ifdef curses
    refresh ();
#endif

    /* Now add any desired payors/payees */
    while ((tmpnum = addpyr ()))
    {
        if (tmpnum == -1)
        {                       /* If error */
            quitrec ();
        }
    }

#ifdef curses
    refresh ();
#endif

    /* Null out all Fields */

    YrTtl.TotXpns = YrTtl.TotInc = 0;
    YrTtl.RRev = EDITION;       /* Set Rev level        */

    for (count = 0; count < 15; count++)
    {
        Income[count].CgyTot = Expense[count].CgyTot = 0;
    }

    for (count = 0; count < 48; count++)
    {
        Person[count].GotFrom = Person[count].PdTo = 0;
    }


    /* At this point, all structures have been set as desired
     * it is now time to write category files and payor files
     */

    if (
           mkdir (jobptr->JobDir, S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH)
        )
    {
        printw ("\nCannot create the directory %s\n", jobptr->JobDir);
        quitrec ();
    }

    if (chdir (jobptr->JobDir))
    {
        printw ("\nCannot CHD to : %s\n", jobptr->JobDir);
        printw ("Error #: %d occurred\n", errno);
        quitrec ();
    }

    if (updatfil () == -1)
    {
        printw ("\nError encountered in creation of auxilliary files...\n");
        printw ("\"newrec\" is now aborting...\n");
        printw ("\nDelete the directory \"%s\"\n", jobptr->JobDir);
        quitrec ();
    }

    /* Create an empty "datafil.rec" */

    makfil (tbuf, datafil);

    if (!(fpath = fopen (tbuf, "w")))
    {
        opn_err (tbuf);
        quitrec ();
    }

    if (fclose (fpath) == -1)
    {
        printw ("Cannot close: %s\n", tbuf);

#ifdef CURSES
        refresh ();
#endif
        return (0);
    }

    /* Now back out of RECORDXXX dir and write to "recstuff" file */

    if (chdir (".."))
    {
        nostuff ("Cannot chdir to \"..\"");
    }
    if (!(fpath = fopen (stuff, "a")))
    {
        nostuff ("Cannot open \"recstuff\" file");
        quitrec ();
    }

    while ((ndx = index (jobptr->JobNam, ' ')))
    {
        *ndx = '~';
    }

    if ((fprintf (fpath, "Job=%s Dir=%s\n", jobptr->JobNam, jobptr->JobDir))
                    == -1)
    {
        nostuff ("Cannot write entry to \"recstuff\" file");
        return (errno);
    }
    if (fclose (fpath) == -1)
    {
        clos_err (stuff);
        return (errno);
    }
   /* jobptr = oldjobptr;*/ /* Restore position of old job ptr */

    printw ("The new job has been successfully installed.\n");
    printw ("Recinit is now concluded.\n");
    printw ("To install another job, rerun recinit.\n");
    printw ("Hit a key...");
    getch ();
/*    quitrec ();*/
    return 0;     /* Useless, but satisfies gcc's prototype requirement */

}                               /* End of main */
