/* Payor functions (for "record").  display list, add to list */

/* $Id$ */

#include "record.h"

void
pyrshow ()
{
    register int count;

    cls;
    printw ("%24s\n\n", "P A Y O R / P A Y E E   L I S T");
    for (count = 0; count < NumPayrs; count++)
    {
        CurXY (scrn, 26 * (count / ((NumPayrs + 2) / 3)),
               (count % ((NumPayrs + 2) / 3)) + 3);
        printw ("%02d-%-22s", count + 1, Person[count].Who);
    }
    CurXY (scrn, 0, 22);
}

/* ******************************************* *
 * addpyr () : add a payor to the the database *
 * Returns:    NumPayrs ( new count of payors) *
 *                on success,                  *
 *             -1 on error                     *
 * ******************************************* */

int
addpyr (void)
{
    char inpstr[sizeof (Person[1].Who) + 1];

    pyrshow ();
    CurXY (scrn, 0, 21);
    printw ("Enter Name for new Payor/Payee...\n<ENTER> alone to exit...");

    if (getnstr (inpstr, sizeof (inpstr)) != ERR)
    {                           /* If a name was added */
        if (strlen (inpstr) == 0)
            return 0;
        strcpy (Person[NumPayrs].Who, inpstr);
        Person[NumPayrs].GotFrom = Person[NumPayrs].PdTo = 0;

        /* If in "record", write this one, else if in "recinit", wait
         *  and write all at one time -- "fwrt" will be a null subroutine
         */
        fwrt (payfil, Person[NumPayrs].Who, sizeof (Person[0]));
        ++NumPayrs;  /* Bump count of payors */
        return (NumPayrs);   /* return 0 to flag success */
    }
    else
    {
        return (-1);            /* Return null if no addition */
    }
}                               /* End of addpyr()   */
