/* Test file to try to successfully load "stdfonts" */

#include <buffs.h>
#include <strings.h>
#include <errno.h>

long _gs_size();

int output;

main()
{
	pflinit();
	output = MakGFX();
}

static FILE *MakGFX()
{
 register FILE *wndw;
 long fsiz;
 char *fntbuf;
 int fontrslt;

	if ( (fontrslt=Font(STDOUT,GRP_FONT,FNT_S8X8)) == ERR ) {
		if (errno != 194 ) {   /* Error NOT Buffer not loaded, return error */
			return ERR;
		}
		else {                    /* Buffer was not loaded, load it */
			int pnum,rstate;
			
			errno = 0;   /* Clear error # */
			
			printf("\nReading \"/dd/sys/stdfonts\"\n");
			
			if ( (pnum=open("/dd/sys/stdfonts",READ)) == ERR ) {
				_errmsg(errno,"/nCannot open 'stdfonts' file\n");
				getchar();
				return ERR;
			}
			
			printf("File read successfully\n");
			printf("doing _gs_sizd()\n");
			
			if( (fsiz = _gs_size(pnum)) < 0x880 ) {
				_errmsg(1,"\nFsize too small - %lx\nShould be %x\n",
						fsiz,0x880);
				getchar();
				return ERR;
			}
			
			printf("Malloc'ing memory (%ld bytes)\n",fsiz);
			
			if( fntbuf = (char *)malloc((int)fsiz) == ERR ) {
				_errmsg(1,"\nCannot allocate memory\n");
				getchar();
				return -1;
			}
			
			printf("Reading /dd/SYS/STDFONTS\n");
			
			if( (rstate=read(pnum,fntbuf,(int)fsiz)) < 0 ) {
				_errmsg(1,"\nError reading font buffer(%d)\n",errno);
				getchar();
				return ERR;
			}
			
			close(pnum);
			NoFnts = TRUE;       /* Had to load fonts, flag it */
		}
	}
	
	printf("Opening new window /w\n");
	if ( !(wndw=fopen("/w","r+")) ) {
		_errmsg(1,"\nCannot open window\n");
		getchar();
		return ERR;
	}
	printf("Writing font buffers to window\n");
	
	if( fontrslt == ERR ) {
		if( write(fileno(wndw),fntbuf,(int)fsiz) < (int)fsiz ) {
			fprintf(stderr,"\nError writing fonts (%d)\n",errno);
			getchar();
		}
		printf("Running free()\n");
		free(fntbuf);
		return ERR;
	}
	
	printf("DWSetting window\n");
	
	if ( DWSet(fileno(wndw),7,0,0,80,24,1,0,0) == ERR ) {
		fclose(wndw);
		_errmsg(1,"\nCannot DWSet window\n");
		getchar();
		return ERR;
	}
	
	printf("MakGFX() run was successful\n");
	
	return (wndw);
}
